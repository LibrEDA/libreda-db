// SPDX-FileCopyrightText: 2023 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Test if blanket implementations work.

//use libreda_db::prelude::*;
//
//fn is_hierarchy_ids<T: HierarchyIds>() {}
//fn is_netlist_ids<T: NetlistIds>() {}
//fn is_layout_ids<T: LayoutIds>() {}
//
//fn is_hierarchy_base<T: HierarchyBase>() {}
//fn is_netlist_base<T: NetlistBase>() {}
//fn is_layout_base<T: LayoutBase>() {}
//fn is_layout_netlist_base<T: L2NBase>() {}
//fn is_hierarchy_edit<T: HierarchyEdit>() {}
//fn is_netlist_edit<T: NetlistEdit>() {}
//fn is_layout_edit<T: LayoutEdit>() {}
//fn is_layout_netlist_edit<T: L2NEdit>() {}
//
//#[test]
//fn test_blanket_impl_ref() {
//    type T<'a> = &'a Chip;
//
//    is_hierarchy_ids::<T>();
//    is_netlist_ids::<T>();
//    is_layout_ids::<T>();
//
//    is_hierarchy_base::<T>();
//    is_netlist_base::<T>();
//    is_layout_base::<T>();
//    is_layout_netlist_base::<T>();
//}
//
//#[test]
//fn test_blanket_impl_rc() {
//    type T = std::rc::Rc<Chip>;
//
//    is_hierarchy_ids::<T>();
//    is_netlist_ids::<T>();
//    is_layout_ids::<T>();
//
//    is_hierarchy_base::<T>();
//    is_netlist_base::<T>();
//    is_layout_base::<T>();
//    is_layout_netlist_base::<T>();
//}
//
//#[test]
//fn test_blanket_impl_arc() {
//    type T = std::sync::Arc<Chip>;
//
//    is_hierarchy_ids::<T>();
//    is_netlist_ids::<T>();
//    is_layout_ids::<T>();
//
//    is_hierarchy_base::<T>();
//    is_netlist_base::<T>();
//    is_layout_base::<T>();
//    is_layout_netlist_base::<T>();
//}
//
//#[test]
//fn test_blanket_impl_mut_ref() {
//    type T<'a> = &'a mut Chip;
//
//    is_hierarchy_ids::<T>();
//    is_hierarchy_base::<T>();
//    is_netlist_base::<T>();
//    is_layout_base::<T>();
//    is_layout_netlist_base::<T>();
//
//    is_hierarchy_edit::<T>();
//    is_netlist_edit::<T>();
//    is_layout_edit::<T>();
//    is_layout_netlist_edit::<T>();
//}
//
//#[test]
//fn test_blanket_impl_box() {
//    type T = Box<Chip>;
//
//    is_hierarchy_ids::<T>();
//    is_hierarchy_base::<T>();
//    is_netlist_base::<T>();
//    is_layout_base::<T>();
//    is_layout_netlist_base::<T>();
//
//    is_hierarchy_edit::<T>();
//    is_netlist_edit::<T>();
//    is_layout_edit::<T>();
//    is_layout_netlist_edit::<T>();
//}
//
