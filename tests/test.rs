// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// use std::fs::File;
// use std::io::Read;

// /// Read a whole file into a byte vector.
// fn read_file(path: &str) -> Vec<u8> {
//     let mut f = File::open(path).unwrap();
//     let mut data = Vec::new();
//     f.read_to_end(&mut data).unwrap();
//     data
// }

// GDS tests
// #[test]
// fn test_load_inverter_cell() {
//     let f = File::open("./tests/data/INVX1.gds").unwrap();
//     let mut reader = BufReader::new(f);
//     let library = gds::Library::read(&mut reader).unwrap();
//     dbg!(&library);
//     println!("{}", library);
//     dbg!(library.name);
//
//     for st in library.structures {
//         dbg!(st.name);
//         for el in st.elements {
//             dbg!(el);
//         }
//     }
// }
//
// #[test]
// fn test_load_gds() {
//     let f = File::open("./tests/data/shapes_and_text.gds").unwrap();
//     let mut reader = BufReader::new(f);
//     let library = gds::Library::read(&mut reader).unwrap();
//     dbg!(&library);
//     println!("{}", library);
//     dbg!(library.name);
//
//     for st in library.structures {
//         dbg!(st.name);
//         for el in st.elements {
//             dbg!(el);
//         }
//     }
// }
//

#[test]
fn test_delegate_core_traits() {
    use libreda_db::prelude::*;

    // Wrap a netlist/layout and delegate
    // the trait implementations to the inner data structure.
    #[allow(unused)]
    struct Wrapper<N> {
        inner: N,
    }

    #[portrait::fill(portrait::delegate(N))]
    impl<N> HierarchyIds for Wrapper<N> where N: HierarchyIds {}

    #[portrait::fill(portrait::delegate(N; self.inner))]
    impl<N> HierarchyBase for Wrapper<N> where N: HierarchyBase {}

    #[portrait::fill(portrait::delegate(N; self.inner))]
    impl<N> HierarchyEdit for Wrapper<N> where N: HierarchyEdit {}

    #[portrait::fill(portrait::delegate(N))]
    impl<N> NetlistIds for Wrapper<N> where N: NetlistIds {}

    #[portrait::fill(portrait::delegate(N; self.inner))]
    impl<N> NetlistBase for Wrapper<N> where N: NetlistBase {}

    #[portrait::fill(portrait::delegate(N; self.inner))]
    impl<N> NetlistEdit for Wrapper<N> where N: NetlistEdit {}

    #[portrait::fill(portrait::delegate(N; self))]
    impl<N> LayoutIds for Wrapper<N> where N: LayoutIds {}

    #[portrait::fill(portrait::delegate(N; self.inner))]
    impl<N> LayoutBase for Wrapper<N> where N: LayoutBase {}

    #[portrait::fill(portrait::delegate(N; self.inner))]
    impl<N> LayoutEdit for Wrapper<N> where N: LayoutEdit {}

    #[portrait::fill(portrait::delegate(N; self.inner))]
    impl<N> L2NBase for Wrapper<N> where N: L2NBase {}

    #[portrait::fill(portrait::delegate(N; self.inner))]
    impl<N> L2NEdit for Wrapper<N> where N: L2NEdit {}
}
