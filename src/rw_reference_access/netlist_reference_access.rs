// SPDX-FileCopyrightText: 2020-2024 Thomas Kramer
// SPDX-License-Identifier: AGPL-3.0-or-later

use libreda_core::netlist::{direction::Direction, terminal_id::TerminalId};

use crate::traits::{NetlistBase, NetlistEdit, NetlistIds};

use super::{CellInstRef, CellRef, RwRefAccess};

/// A reference to a net.
/// This is just a wrapper around a netlist and a net ID.
#[derive(PartialEq, Eq, Hash)]
pub struct NetRef<N: NetlistIds> {
    /// Reference to the parent data structure.
    pub(super) base: RwRefAccess<N>,
    /// ID of the corresponding net.
    pub(super) id: N::NetId,
}

impl<N: NetlistIds> Clone for NetRef<N> {
    fn clone(&self) -> Self {
        Self {
            base: self.base.clone(),
            id: self.id.clone(),
        }
    }
}

/// A reference to a pin.
/// This is just a wrapper around a netlist and a pin ID.
#[derive(Hash, Eq, PartialEq)]
pub struct PinRef<N: NetlistIds> {
    /// Reference to the parent data structure.
    pub(super) base: RwRefAccess<N>,
    /// ID of the corresponding pin.
    pub(super) id: N::PinId,
}

impl<N: NetlistIds> Clone for PinRef<N> {
    fn clone(&self) -> Self {
        Self {
            base: self.base.clone(),
            id: self.id.clone(),
        }
    }
}

/// A reference to a pin instance.
/// This is just a wrapper around a netlist and a pin instance ID.
#[derive(Hash, Eq, PartialEq)]
pub struct PinInstRef<N: NetlistIds> {
    /// Reference to the parent data structure.
    pub(super) base: RwRefAccess<N>,
    /// ID of the corresponding pin instance.
    pub(super) id: N::PinInstId,
}

impl<N: NetlistIds> Clone for PinInstRef<N> {
    fn clone(&self) -> Self {
        Self {
            base: self.base.clone(),
            id: self.id.clone(),
        }
    }
}

// /// A reference to a terminal.
// /// This is just a wrapper around a netlist and a terminal ID.
// #[derive(Clone)]
// pub struct TerminalRef<N: NetlistIds> {
//     /// Reference to the parent data structure.
//     pub(super) base: RwRefAccess<N>,
//     /// ID of the corresponding pin instance.
//     pub(super) id: TerminalId<N>,
// }

/// Either a pin or a pin instance.
#[derive(Clone, Hash, Eq, PartialEq)]
pub enum TerminalRef<N: NetlistIds> {
    /// A template pin.
    Pin(PinRef<N>),
    /// An instance of a pin.
    PinInst(PinInstRef<N>),
}

impl<N: NetlistIds> From<PinRef<N>> for TerminalRef<N> {
    fn from(p: PinRef<N>) -> Self {
        Self::Pin(p)
    }
}

impl<N: NetlistIds> From<PinInstRef<N>> for TerminalRef<N> {
    fn from(p: PinInstRef<N>) -> Self {
        Self::PinInst(p)
    }
}

impl<N: NetlistBase> From<TerminalRef<N>> for TerminalId<N> {
    fn from(t: TerminalRef<N>) -> Self {
        match t {
            TerminalRef::Pin(p) => TerminalId::PinId(p.id),
            TerminalRef::PinInst(p) => TerminalId::PinInstId(p.id),
        }
    }
}

impl<N: NetlistBase> CellRef<N> {
    /// Get the number of nets inside this cell.
    pub fn num_nets(&self) -> usize {
        self.base.read().num_internal_nets(&self.id)
    }

    /// Get the number of pins of this cell.
    pub fn num_pins(&self) -> usize {
        self.base.read().num_pins(&self.id)
    }

    /// Get the net which represents the constant 0.
    pub fn net_zero(&self) -> NetRef<N> {
        let id = self.base.read().net_zero(&self.id);
        NetRef {
            base: self.base.clone(),
            id,
        }
    }

    /// Get the net which represents the constant 1.
    pub fn net_one(&self) -> NetRef<N> {
        let id = self.base.read().net_one(&self.id);
        NetRef {
            base: self.base.clone(),
            id,
        }
    }

    /// Get a vector with all nets.
    pub fn each_net(&self) -> Vec<NetRef<N>> {
        self.base
            .read()
            .each_internal_net(&self.id)
            .map(|id| NetRef {
                base: self.base.clone(),
                id,
            })
            .collect()
    }

    /// Get a vector with all pins.
    pub fn each_pin(&self) -> Vec<PinRef<N>> {
        self.base
            .read()
            .each_pin(&self.id)
            .map(|id| PinRef {
                base: self.base.clone(),
                id,
            })
            .collect()
    }

    /// Find a net by its name.
    pub fn net_by_name(&self, name: &str) -> Option<NetRef<N>> {
        self.base
            .read()
            .net_by_name(&self.id, name)
            .map(|id| NetRef {
                base: self.base.clone(),
                id,
            })
    }

    /// Find a pin by its name.
    pub fn pin_by_name(&self, name: &str) -> Option<PinRef<N>> {
        self.base
            .read()
            .pin_by_name(&self.id, name)
            .map(|id| PinRef {
                base: self.base.clone(),
                id,
            })
    }
}

impl<N: NetlistEdit> CellRef<N> {
    /// Create a new pin and append it at the end of the list of pins.
    pub fn create_pin(&self, name: N::NameType, direction: Direction) -> PinRef<N> {
        let id = self.base.write().create_pin(&self.id, name, direction);

        PinRef {
            base: self.base.clone(),
            id,
        }
    }
    /// Create a new net.
    pub fn create_net(&self, name: Option<N::NameType>) -> NetRef<N> {
        let id = self.base.write().create_net(&self.id, name);

        NetRef {
            base: self.base.clone(),
            id,
        }
    }

    /// Remove the net from the cell.
    pub fn remove_net(&self, net: &N::NetId) {
        self.base.write().remove_net(net);
    }
}

impl<N: NetlistBase> CellInstRef<N> {
    /// Get a vector with all pin instances.
    pub fn each_pin_instance(&self) -> Vec<PinInstRef<N>> {
        self.base
            .read()
            .each_pin_instance(&self.id)
            .map(|id| PinInstRef {
                base: self.base.clone(),
                id,
            })
            .collect()
    }

    /// Get the pin instance which corresponds to the given template pin.
    pub fn pin_inst(&self, template_pin: &PinRef<N>) -> PinInstRef<N> {
        debug_assert_eq!(
            self.template_id(),
            template_pin.parent().id(),
            "specified pin does not exist for this cell type"
        );

        let id = self.base.write().pin_instance(&self.id, &template_pin.id);
        PinInstRef {
            base: self.base.clone(),
            id,
        }
    }

    /// Get a vector with all external nets connected to this pin instance.
    pub fn each_net(&self) -> Vec<NetRef<N>> {
        self.base
            .read()
            .each_external_net(&self.id)
            .map(|id| NetRef {
                base: self.base.clone(),
                id,
            })
            .collect()
    }
}

impl<N: NetlistBase> PinRef<N> {
    /// Get the name of the pin.
    pub fn name(&self) -> N::NameType {
        self.base.read().pin_name(&self.id)
    }

    /// Create a qualified name.
    /// For pins: 'cell_name:pin_name'
    pub fn qname(&self, separator: &str) -> String {
        format!("{}{}{}", self.parent().name(), separator, self.name())
    }

    /// Get the internal net connected to this pin, if any.
    pub fn net(&self) -> Option<NetRef<N>> {
        self.base.read().net_of_pin(&self.id).map(|id| NetRef {
            base: self.base.clone(),
            id,
        })
    }

    /// Get the ID of the parent cell of this pin.
    pub fn parent_id(&self) -> N::CellId {
        self.base.read().parent_cell_of_pin(&self.id)
    }

    /// Get the parent cell of this pin.
    pub fn parent(&self) -> CellRef<N> {
        CellRef {
            base: self.base.clone(),
            id: self.parent_id(),
        }
    }

    /// Get the pin instance which corresponds to this pin.
    pub fn instance(&self, cell_inst: &CellInstRef<N>) -> PinInstRef<N> {
        cell_inst.pin_inst(self)
    }
}

impl<N: NetlistEdit> PinRef<N> {
    /// Get connect a net to this pin and get the previously connected net.
    pub fn connect(&self, net: Option<NetRef<N>>) -> Option<NetRef<N>> {
        self.base
            .write()
            .connect_pin(&self.id, net.map(|n| n.id))
            .map(|id| NetRef {
                base: self.base.clone(),
                id,
            })
    }
}

impl<N: NetlistBase> PinInstRef<N> {
    /// Get the template pin of this pin instance.
    pub fn template(&self) -> PinRef<N> {
        PinRef {
            base: self.base.clone(),
            id: self.template_id(),
        }
    }

    /// Get the ID of the template pin of this pin instance.
    pub fn template_id(&self) -> N::PinId {
        self.base.read().template_pin(&self.id)
    }

    /// Get the cell instance which contains this pin instance.
    pub fn parent(&self) -> CellInstRef<N> {
        CellInstRef {
            base: self.base.clone(),
            id: self.parent_id(),
        }
    }

    /// Get the ID of the cell instance which contains this pin instance.
    pub fn parent_id(&self) -> N::CellInstId {
        self.base.read().parent_of_pin_instance(&self.id)
    }

    /// Get the net connected to this pin instance, if any.
    pub fn net(&self) -> Option<NetRef<N>> {
        self.base
            .read()
            .net_of_pin_instance(&self.id)
            .map(|id| NetRef {
                base: self.base.clone(),
                id,
            })
    }
}

impl<N: NetlistEdit> PinInstRef<N> {
    /// Get connect a net to this pin and get the previously connected net.
    pub fn connect(&self, net: Option<NetRef<N>>) -> Option<NetRef<N>> {
        self.base
            .write()
            .connect_pin_instance(&self.id, net.map(|n| n.id))
            .map(|id| NetRef {
                base: self.base.clone(),
                id,
            })
    }
}

impl<N: NetlistBase> NetRef<N> {
    /// Get the name of the net.
    pub fn name(&self) -> Option<N::NameType> {
        self.base.read().net_name(&self.id)
    }

    /// Get the ID of the cell which contains this net.
    pub fn parent_id(&self) -> N::CellId {
        self.base.read().parent_cell_of_net(&self.id)
    }

    /// Get the cell which contains this net.
    pub fn parent(&self) -> CellRef<N> {
        CellRef {
            base: self.base.clone(),
            id: self.parent_id(),
        }
    }

    /// Check if this net is the constant 0.
    pub fn is_zero(&self) -> bool {
        let b = self.base.read();
        b.net_zero(&b.parent_cell_of_net(&self.id)) == self.id
    }

    /// Check if this net is the constant 1.
    pub fn is_one(&self) -> bool {
        let b = self.base.read();
        b.net_one(&b.parent_cell_of_net(&self.id)) == self.id
    }

    /// Get a list of all terminals attached to this net.
    pub fn each_terminal(&self) -> Vec<TerminalRef<N>> {
        let b = self.base.read();
        let pins = b.each_pin_of_net(&self.id).map(|id| {
            PinRef {
                base: self.base.clone(),
                id,
            }
            .into()
        });
        let pin_insts = b.each_pin_instance_of_net(&self.id).map(|id| {
            PinInstRef {
                base: self.base.clone(),
                id,
            }
            .into()
        });

        pins.chain(pin_insts).collect()
    }

    /// Get a vector with all pins connected to this net.
    pub fn each_pin(&self) -> Vec<PinRef<N>> {
        self.base
            .read()
            .each_pin_of_net(&self.id)
            .map(|id| PinRef {
                base: self.base.clone(),
                id,
            })
            .collect()
    }

    /// Get a vector with all instances connected to this net.
    pub fn each_pin_instance(&self) -> Vec<PinInstRef<N>> {
        self.base
            .read()
            .each_pin_instance_of_net(&self.id)
            .map(|id| PinInstRef {
                base: self.base.clone(),
                id,
            })
            .collect()
    }

    /// Get the number of pins connected to this net.
    /// Does not include pin instances.
    /// See also `num_pin_instances()` and `num_terminals()`.
    pub fn num_pins(&self) -> usize {
        self.base.read().num_net_pins(&self.id)
    }

    /// Get the number of pin instances connected to this net.
    /// Does not include pins.
    /// See also `num_pins()` and `num_terminals()`.
    pub fn num_pin_instances(&self) -> usize {
        self.base.read().num_net_pin_instances(&self.id)
    }

    /// Get the number of terminals attached to this net.
    pub fn num_terminals(&self) -> usize {
        self.base.read().num_net_terminals(&self.id)
    }
}

impl<N: NetlistEdit> NetRef<N> {
    /// Connect this net to the given pin instance.
    pub fn connect_pin_inst(&self, pin: PinInstRef<N>) {
        pin.connect(Some(self.clone()));
    }
    /// Connect this net to the given pin instance.
    pub fn connect_pin(&self, pin: PinRef<N>) {
        pin.connect(Some(self.clone()));
    }
}

impl<N: NetlistBase> TerminalRef<N> {
    /// Get the net connected to this terminal.
    pub fn net(&self) -> Option<NetRef<N>> {
        match self {
            TerminalRef::Pin(p) => p.net(),
            TerminalRef::PinInst(p) => p.net(),
        }
    }
}

impl<N: NetlistEdit> TerminalRef<N> {
    /// Get connect a net to this terminal and get the previously connected net.
    pub fn connect(&self, net: Option<NetRef<N>>) -> Option<NetRef<N>> {
        match self {
            TerminalRef::Pin(p) => p.connect(net),
            TerminalRef::PinInst(p) => p.connect(net),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::chip::Chip;
    use crate::prelude::*;
    use crate::rw_reference_access::RwRefAccess;

    #[test]
    fn test_create_circuit() {
        let chip = RwRefAccess::new(Chip::new());

        let a = chip.create_cell("A".into());
        let b = chip.create_cell("B".into());

        let pin_aa = a.create_pin("A".into(), Direction::Input);
        let _pin_ab = a.create_pin("B".into(), Direction::Input);
        let _pin_az = a.create_pin("Z".into(), Direction::Output);
        assert_eq!(a.num_pins(), 3);
        assert_eq!(pin_aa.qname(":"), "A:A");

        let pin_ba = b.create_pin("A".into(), Direction::Input);

        let inst = a.create_instance(&b, Some("sub_inst".into()));
        assert_eq!(inst.parent().id(), a.id());

        let net = a.create_net(Some("net1".into()));
        assert_eq!(a.num_nets(), 1 + 2);

        inst.pin_inst(&pin_ba).connect(Some(net.clone()));
        assert_eq!(net.num_terminals(), 1);
        assert_eq!(net.num_pin_instances(), 1);
        assert_eq!(net.num_pins(), 0);
        net.connect_pin(pin_aa);
        assert_eq!(net.num_terminals(), 2);
        assert_eq!(net.num_pin_instances(), 1);
        assert_eq!(net.num_pins(), 1);
    }
}
