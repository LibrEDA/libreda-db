// SPDX-FileCopyrightText: 2022-2024 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Container with `O(1)` insertion, lookup and remove operations.
//!
//! Insertion of an element returns a key.
//! Keys may be reused once an element is removed. Each element is associated with a ID which
//! can be used for making keys unique even if a storage position is reused. However, this comes at the
//! cost of some storage overhead. Using an `IdType` of [`ZeroBitCounter`] effectively deactivates unique IDs but also
//! does not use more storage.

#![allow(unused)]

use std::ops::Add;

use num_traits::{Bounded, FromPrimitive, PrimInt, ToPrimitive, Unsigned, Zero};

#[derive(Debug, Default, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
struct Element<IndexType, IdType, T> {
    /// Identifier which will be incremented once this storage positions get reused.
    /// This allows to create unique IDs for elements which are stored in a location which was previously already used.
    /// Typically this type is an unsigned integer or a [`ZeroBitCounter`].
    id: IdType,
    /// Actual payload data.
    value: OccupiedFree<T, IndexType>,
}

#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
enum OccupiedFree<T, IndexType> {
    /// An element is present at this position.
    Occupied(T),
    /// No element is present at this position.
    /// Point to the next free element.
    /// If there is no next free element, points to itself.
    Free { next_free: IndexType },
}

impl<T, I> OccupiedFree<T, I> {
    /// Get a reference to the value.
    fn as_ref(&self) -> Option<&T> {
        match self {
            Self::Occupied(t) => Some(t),
            _ => None,
        }
    }

    /// Get a mutable reference to the value.
    fn as_mut(&mut self) -> Option<&mut T> {
        match self {
            Self::Occupied(t) => Some(t),
            _ => None,
        }
    }

    /// Remove the value and mark the place as 'free'.
    fn take(&mut self) -> Option<T>
    where
        I: Default,
    {
        match std::mem::take(self) {
            OccupiedFree::Occupied(t) => Some(t),
            _ => None,
        }
    }

    fn is_free(&self) -> bool {
        matches!(self, Self::Free { next_free: _ })
    }
}

impl<T, IndexType: Default> Default for OccupiedFree<T, IndexType> {
    fn default() -> Self {
        OccupiedFree::Free {
            next_free: Default::default(),
        }
    }
}

/// Slab allocator with 8-bit indices. Can hold at most 2^8 elements.
pub type SlabAlloc8<T> = SlabAlloc<T, u8, u8>;
/// Slab allocator with 16-bit indices. Can hold at most 2^16 elements.
pub type SlabAlloc16<T> = SlabAlloc<T, u16, u16>;
/// Slab allocator with 32-bit indices. Can hold at most 2^32 elements.
pub type SlabAlloc32<T> = SlabAlloc<T, u32, u32>;
/// Slab allocator with 64-bit indices. Can hold at most 2^64 elements.
pub type SlabAlloc64<T> = SlabAlloc<T, u64, u64>;
/// Slab allocator with n-bit indices. Can hold at most 2^n elements.
/// Where `n` is the machine word size.
pub type SlabAllocUsize<T> = SlabAlloc<T, usize, usize>;

pub type SlabIndex8 = SlabIndex<u8, u8>;
pub type SlabIndex16 = SlabIndex<u16, u16>;
pub type SlabIndex32 = SlabIndex<u32, u32>;
pub type SlabIndex64 = SlabIndex<u64, u64>;
pub type SlabIndexUsize = SlabIndex<usize, usize>;

/// Container which efficiently allocates space for its elements.
#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct SlabAlloc<T, IndexType = u32, IdType = ZeroBitCounter> {
    data: Vec<Element<IndexType, IdType, T>>,
    /// Pointer to free element. Each free element points to the next free element.
    /// A value of `usize::MAX` marks the end of the list.
    free_position: usize,
    /// Number of elements currently in the map.
    len: usize,
}

impl<T, Idx, Id> Default for SlabAlloc<T, Idx, Id> {
    fn default() -> Self {
        Self {
            data: vec![],
            free_position: usize::MAX,
            len: 0,
        }
    }
}

/// Key into an [`SlabAlloc`].
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct SlabIndex<IndexType, IdType = ZeroBitCounter> {
    /// Pointer into the data array.
    index: IndexType,
    /// A monotonically increasing ID which makes sure that the same index is not repeated.
    id: IdType,
}

// TODO
// /// Key into an [`SlabAlloc`].
// #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
// #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
// pub struct SlabIndexV2<const ID_BITS: usize = 0> {
//     index: NonZero<usize>,
// }

impl<IndexType, IdType> SlabIndex<IndexType, IdType>
where
    IndexType: PrimInt + Unsigned + ToPrimitive,
{
    pub(crate) fn index(&self) -> usize {
        self.index.to_usize().unwrap()
    }
}

impl<T, IndexType, IdType> std::ops::Index<SlabIndex<IndexType, IdType>>
    for SlabAlloc<T, IndexType, IdType>
where
    IndexType: PrimInt + Unsigned + ToPrimitive + FromPrimitive,
    IdType: IdCounter,
{
    type Output = T;

    fn index(&self, index: SlabIndex<IndexType, IdType>) -> &Self::Output {
        self.get(index).expect("invalid index")
    }
}

impl<T, IndexType, IdType> std::ops::IndexMut<SlabIndex<IndexType, IdType>>
    for SlabAlloc<T, IndexType, IdType>
where
    IndexType: PrimInt + Unsigned + ToPrimitive + FromPrimitive,
    IdType: IdCounter,
{
    fn index_mut(&mut self, index: SlabIndex<IndexType, IdType>) -> &mut Self::Output {
        self.get_mut(index).expect("invalid index")
    }
}

#[allow(unused)]
impl<T, IndexType, IdType> SlabAlloc<T, IndexType, IdType>
where
    IndexType: PrimInt + Unsigned + ToPrimitive + FromPrimitive,
    IdType: IdCounter,
{
    /// Create an empty container.
    pub fn new() -> Self {
        Self {
            data: vec![],
            len: 0,
            free_position: usize::MAX,
        }
    }

    /// Access an element.
    pub fn get(&self, index: SlabIndex<IndexType, IdType>) -> Option<&T> {
        if let Some(entry) = self.data.get(index.index()) {
            if entry.id == index.id {
                entry.value.as_ref()
            } else {
                None
            }
        } else {
            None
        }
    }

    pub fn get_mut_or_default(&mut self, index: SlabIndex<IndexType, IdType>) -> &mut T
    where
        T: Default,
    {
        let entry = &mut self.data[index.index()];
        let id_matches = entry.id == index.id;
        if id_matches {
            if entry.value.is_free() {
                entry.value = OccupiedFree::Occupied(Default::default());
                entry.value.as_mut().unwrap()
            } else {
                entry.value.as_mut().unwrap()
            }
        } else {
            panic!("index is invalid");
        }
    }

    /// Access an element.
    pub fn get_mut(&mut self, index: SlabIndex<IndexType, IdType>) -> Option<&mut T> {
        if let Some(entry) = self.data.get_mut(index.index()) {
            if entry.id == index.id {
                entry.value.as_mut()
            } else {
                None
            }
        } else {
            None
        }
    }
    /// Get mutable acces to two different element.
    /// Panics if the indices point to the same element.
    pub fn get_mut2(&mut self, index: [SlabIndex<IndexType, IdType>; 2]) -> [Option<&mut T>; 2] {
        let offsets = index.map(|i| i.index());
        assert_ne!(offsets[0], offsets[1], "indices point to the same element");
        let split_idx = offsets[0].max(offsets[1]);
        let (slice_low, slice_high) = self.data.split_at_mut(split_idx);

        // Get mutable access to 2 elements.
        let entries = if offsets[1] == split_idx {
            debug_assert!(offsets[0] < offsets[1]);
            [&mut slice_low[offsets[0]], &mut slice_high[0]]
        } else {
            debug_assert!(offsets[1] < offsets[0]);
            [&mut slice_high[0], &mut slice_low[offsets[1]]]
        };

        let mut i = 0;
        entries.map(|entry| {
            let expected_id = index[i].id;
            i += 1;
            if entry.id == expected_id {
                entry.value.as_mut()
            } else {
                // Index is invalid.
                None
            }
        })
    }

    pub fn contains_key(&self, index: SlabIndex<IndexType, IdType>) -> bool {
        self.get(index).is_some()
    }

    fn next_free_index(&mut self) -> Option<usize> {
        if self.free_position == usize::MAX {
            None
        } else {
            let next_next = match &self.data[self.free_position].value {
                OccupiedFree::Occupied(_) => panic!("entry is supposed to be free"),
                OccupiedFree::Free { next_free } => {
                    let n = next_free.to_usize().unwrap();

                    if n == self.free_position {
                        // End of list.
                        usize::MAX
                    } else {
                        n
                    }
                }
            };

            let output = self.free_position;
            self.free_position = next_next;
            Some(output)
        }
    }

    fn push_free_index(&mut self, fresh_idx: usize) {
        let old_front = self.free_position;

        // Create link: free position -> fresh index
        self.free_position = fresh_idx;

        // Create link: fresh element -> None
        match &mut self.data[fresh_idx].value {
            OccupiedFree::Occupied(_) => panic!("entry is supposed to be free"),
            OccupiedFree::Free { next_free } => {
                // The last element in the list points to itself.
                let next = if old_front == usize::MAX {
                    // Point to itself to mark the end of the list.
                    fresh_idx
                } else {
                    old_front
                };
                *next_free = IndexType::from_usize(next).unwrap()
            }
        }
    }

    /// Insert an element and return the index of it.
    ///
    /// # Panics
    /// Panics when the amount of indices is exhausted. This happens
    /// when there are already `IndexType::max_value()` elements in the container.
    pub fn insert(&mut self, value: T) -> SlabIndex<IndexType, IdType> {
        // Find a new free index.
        let index = if let Some(index) = self.next_free_index() {
            let entry = self.data.get_mut(index).unwrap();
            debug_assert!(entry.value.is_free());
            entry.value = OccupiedFree::Occupied(value);
            let index = IndexType::from_usize(index).expect("slab allocator: out of indices");
            SlabIndex {
                index,
                id: entry.id,
            }
        } else {
            // Push new element.
            let idx =
                IndexType::from_usize(self.data.len()).expect("slab allocator: out of indices");
            let id = IdType::zero();
            self.data.push(Element {
                id,
                value: OccupiedFree::Occupied(value),
            }); // Extend by one element.
            SlabIndex { index: idx, id }
        };

        self.len += 1;

        index
    }

    /// Try to insert the value at the given index.
    /// Succeeds if the position is free and returns the index.
    /// Fails if the position is occupied and returns the provided value.
    ///
    /// Complexity: O(number of free positions)
    pub fn insert_at(
        &mut self,
        index: SlabIndex<IndexType, IdType>,
        value: T,
    ) -> Result<SlabIndex<IndexType, IdType>, T> {
        let pos = index.index();

        if pos >= self.data.len() {
            // Insert at position beyond the current length of the array.
            let original_len = self.data.len();
            // Append new positions. Fill with dummy elements.
            let dummy = || Element {
                id: Zero::zero(),
                value: OccupiedFree::Free {
                    next_free: Zero::zero(),
                },
            };
            self.data.resize_with(pos + 1, dummy);

            for i in original_len..pos {
                self.push_free_index(i);
            }

            // Store the value.
            self.data[pos].id = index.id;
            self.data[pos].value = OccupiedFree::Occupied(value);
            Ok(index)
        } else {
            // Insert at position within the length of the array.
            if (self.data[pos].value.is_free()) {
                // Find the predecessor of `pos` in the linked list of free positions
                // and unlink the element at `pos` from the list of free positions.
                if (pos == self.free_position) {
                    // Happens to be the first element in the list.
                    // Unlink.
                    let idx = self.next_free_index(); // Consume the free element.
                    debug_assert_eq!(idx, Some(pos));
                } else {
                    let mut pointer = self.free_position;
                    if pointer == usize::MAX {
                        // End of list.
                        unreachable!("Did not find the position in the list of free elements. But it should be there.");
                    }
                    let predecessor = loop {
                        let next_pointer = match &self.data[pointer].value {
                            OccupiedFree::Occupied(_) => unreachable!(
                                "list of free elements can't contain an occupied element"
                            ),
                            OccupiedFree::Free { next_free } => next_free.to_usize().unwrap(),
                        };
                        if next_pointer == pointer {
                            // Reached end of list.
                            unreachable!("Did not find the position in the list of free elements. But it should be there.");
                        }
                        if next_pointer == pos {
                            // Found the predecessor of the element at `pos`.
                            break pointer;
                        }
                    };
                    // Unlink the element at `pos` from the linked list of free positions.
                    let successor = match &self.data[pos].value {
                        OccupiedFree::Occupied(_) => unreachable!(),
                        OccupiedFree::Free { next_free } => *next_free,
                    };
                    match &mut self.data[predecessor].value {
                        OccupiedFree::Occupied(_) => unreachable!(),
                        OccupiedFree::Free { next_free } => *next_free = successor,
                    }
                }

                // Store the value.
                self.data[pos].id = index.id;
                self.data[pos].value = OccupiedFree::Occupied(value);
                Ok(index)
            } else {
                // Can't use position because is occupied.
                Err(value)
            }
        }
    }

    /// Allocate space to hold `n` elements.
    pub fn reserve(&mut self, n: usize) {
        self.data.reserve(n);
    }

    /// Remove an element.
    pub fn remove(&mut self, index: SlabIndex<IndexType, IdType>) -> Option<T>
    where
        IndexType: Default,
    {
        let entry = self.data.get_mut(index.index())?;

        let (old_value, free_index) = if entry.id == index.id {
            // ID matches.
            let old_value = entry.value.take()?;

            self.len -= 1;

            // Make sure the next allocation uses a fresh ID value for this index.
            entry.id = if entry.id < IdType::max_value() {
                entry.id.increment()
            } else {
                IdType::zero() // wrapping addition
            };

            (Some(old_value), Some(index.index()))
        } else {
            (None, None)
        };

        if let Some(i) = free_index {
            self.push_free_index(i);
        }

        old_value
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn is_empty(&self) -> bool {
        self.len == 0
    }

    /// Reclaim as much space as possible.
    pub fn shrink_to_fit(&mut self) {
        // Remove free places from the end of the array.
        while self
            .data
            .last()
            .map(|entry| entry.value.is_free())
            .unwrap_or(false)
        {
            self.data.pop();
        }
        self.data.shrink_to_fit();

        // Rebuild the linked list.

        self.free_position = usize::MAX;

        let mut previous_free = None;

        for (i, e) in self.data.iter_mut().enumerate() {
            if let OccupiedFree::Free { next_free } = &mut e.value {
                self.free_position = i;
                *next_free = if let Some(p) = previous_free.take() {
                    IndexType::from_usize(p).unwrap()
                } else {
                    IndexType::from_usize(i).unwrap() // Mark as last free element.
                };
                previous_free = Some(next_free.to_usize().unwrap());
            }
        }
    }

    /// Drop all data.
    pub fn clear(&mut self) {
        self.data.clear();
        self.free_position = usize::MAX;
        self.len = 0;
    }

    /// Iterate over all key-value pairs.
    pub fn iter(&self) -> impl Iterator<Item = (SlabIndex<IndexType, IdType>, &T)> {
        self.data.iter().enumerate().filter_map(|(index, entry)| {
            let index = IndexType::from_usize(index).unwrap();
            entry.value.as_ref().map(|v| {
                (
                    SlabIndex {
                        index,
                        id: entry.id,
                    },
                    v,
                )
            })
        })
    }

    /// Iterate over all key-value pairs.
    pub fn into_iter(self) -> impl Iterator<Item = (SlabIndex<IndexType, IdType>, T)> {
        self.data
            .into_iter()
            .enumerate()
            .filter_map(|(index, entry)| {
                let index = IndexType::from_usize(index).unwrap();
                match entry.value {
                    OccupiedFree::Occupied(v) => Some((
                        SlabIndex {
                            index,
                            id: entry.id,
                        },
                        v,
                    )),
                    _ => None,
                }
            })
    }

    /// Iterate over all values.
    pub fn into_values(self) -> impl Iterator<Item = T> {
        self.into_iter().map(|(_, v)| v)
    }

    /// Iterate over all key-value pairs.
    pub fn iter_mut(&mut self) -> impl Iterator<Item = (SlabIndex<IndexType, IdType>, &mut T)> {
        self.data
            .iter_mut()
            .enumerate()
            .filter_map(|(index, entry)| {
                entry.value.as_mut().map(|v| {
                    let index = IndexType::from_usize(index).unwrap();
                    (
                        SlabIndex {
                            index,
                            id: entry.id,
                        },
                        v,
                    )
                })
            })
    }

    /// Iterate over all values in the map.
    pub fn values(&self) -> impl Iterator<Item = &T> {
        self.data.iter().filter_map(|entry| entry.value.as_ref())
    }

    /// Iterate over all mutable values in the map.
    pub fn values_mut(&mut self) -> impl Iterator<Item = &mut T> {
        self.data
            .iter_mut()
            .filter_map(|entry| entry.value.as_mut())
    }

    /// Iterate over all keys in the map.
    pub fn keys(&self) -> impl Iterator<Item = SlabIndex<IndexType, IdType>> + '_ {
        self.data.iter().enumerate().filter_map(|(index, entry)| {
            entry.value.as_ref().map(|_| {
                let index = IndexType::from_usize(index).unwrap();
                SlabIndex {
                    index,
                    id: entry.id,
                }
            })
        })
    }
}

#[test]
fn test_slab_allocator() {
    let mut map: SlabAlloc<_, u32, u32> = SlabAlloc::new();

    let h1 = map.insert(1);
    let h2 = map.insert(2);

    assert_eq!(map.get(h1), Some(&1));
    assert_eq!(map.get(h2), Some(&2));

    assert_eq!(map.remove(h1), Some(1));

    let h3 = map.insert(3);
    assert_eq!(map.data.len(), 2, "position of 1 was reused");
    assert_eq!(map.get(h1), None);
    assert_eq!(map.get(h3), Some(&3));

    assert_eq!(map.iter().count(), 2);
}

#[test]
fn test_slab_allocator_mut2() {
    let mut map: SlabAlloc<_, u32, u32> = SlabAlloc::new();

    let h1 = map.insert(1);
    let h2 = map.insert(2);

    let [a, b] = map.get_mut2([h1, h2]);
    assert_eq!(a, Some(&mut 1));
    assert_eq!(b, Some(&mut 2));

    let [a, b] = map.get_mut2([h2, h1]);
    assert_eq!(a, Some(&mut 2));
    assert_eq!(b, Some(&mut 1));
}

#[test]
fn test_slab_allocator_double_remove() {
    let mut map: SlabAlloc<_, u32, u32> = SlabAlloc::new();

    let h1 = map.insert(1);

    map.remove(h1);
    assert_eq!(map.remove(h1), None);
}

#[test]
fn test_slab_allocator_remove() {
    // Remove and insert elements.
    // Freed places in the list should be reused.
    let mut map: SlabAlloc<_, u32, u32> = SlabAlloc::new();

    let h1 = map.insert(1);
    let h2 = map.insert(2);
    let h3 = map.insert(3);

    assert_eq!(map.len(), 3);
    assert_eq!(map.data.len(), 3);

    assert_eq!(map.remove(h2), Some(2));
    assert_eq!(map.len(), 2);
    assert_eq!(map.data.len(), 3);

    let h4 = map.insert(4);
    assert_eq!(map.len(), 3);
    assert_eq!(map.data.len(), 3);

    map.remove(h1);
    map.remove(h1); // Double-remove
    map.remove(h3);
    assert_eq!(map.len(), 1);
    assert_eq!(map.data.len(), 3);

    let h5 = map.insert(5);
    assert_eq!(map.len(), 2);
    assert_eq!(map.data.len(), 3);
    let h6 = map.insert(6);
    assert_eq!(map.len(), 3);
    assert_eq!(map.data.len(), 3);
    let h7 = map.insert(7);
    assert_eq!(map.len(), 4);
    assert_eq!(map.data.len(), 4);

    assert_eq!(map[h4], 4);
    assert_eq!(map[h5], 5);
    assert_eq!(map[h6], 6);
    assert_eq!(map[h7], 7);
}

#[test]
fn test_slab_insert_at() {
    let mut map: SlabAlloc<_, u32, u32> = SlabAlloc::new();

    let h1 = map.insert(1);
    map.remove(h1);
    assert_eq!(map.free_position, 0);

    let h1b = map.insert_at(h1, 1);
    assert_eq!(h1b, Ok(h1));
    assert_eq!(map.free_position, usize::MAX);
}

#[test]
fn test_slab_allocator_shrink() {
    let mut map: SlabAlloc<_, u32, u32> = SlabAlloc::new();

    let h1 = map.insert(1);
    let h2 = map.insert(2);
    let h3 = map.insert(3);

    assert_eq!(map.len(), 3);
    assert_eq!(map.data.len(), 3);
    map.remove(h1); // wont be shrinked because is at the beginning of the array
    map.remove(h3); // will be shrinked because is at end of the array
    assert_eq!(map.len(), 1);
    assert_eq!(map.data.len(), 3);
    map.shrink_to_fit();
    assert_eq!(map.len(), 1);
    assert_eq!(map.data.len(), 2);

    let mut map2 = map.clone(); // make an independent clone for another test.

    // Check if insertion still works normally after shrinking.
    let h4 = map.insert(4);
    let h5 = map.insert(5);

    assert_eq!(map[h4], 4);
    assert_eq!(map[h5], 5);
    assert_eq!(map.len(), 3);
    assert_eq!(map.data.len(), 3);

    // Check if removal of last element still works after shrinking.
    map2.remove(h2);
    assert_eq!(map2.len(), 0);
    assert_eq!(map2.data.len(), 2);
    map2.insert(4);
    map2.insert(5);
    assert_eq!(map2.len(), 2);
    assert_eq!(map2.data.len(), 2);
    // List of free places should be empty now.
    assert_eq!(map2.free_position, usize::MAX);
}

/// Abstraction of a type which can be used as a counter.
/// Used to make the slab allocator generic in the number of bits used for ID counters.
pub trait IdCounter: PartialEq + Eq + Zero + Bounded + PartialOrd + Copy {
    fn increment(self) -> Self;
}

/// 0-bit unsigned integer.
/// Used as a 0-bit ID.
#[derive(Copy, Clone, PartialEq, Eq, Hash, Ord, PartialOrd, Debug, Default)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct ZeroBitCounter;

impl Add for ZeroBitCounter {
    type Output = Self;

    fn add(self, _rhs: Self) -> Self::Output {
        Self
    }
}

impl Zero for ZeroBitCounter {
    fn zero() -> Self {
        Self
    }

    fn is_zero(&self) -> bool {
        true
    }
}

impl Bounded for ZeroBitCounter {
    fn min_value() -> Self {
        Self
    }

    fn max_value() -> Self {
        Self
    }
}

impl IdCounter for ZeroBitCounter {
    fn increment(self) -> Self {
        Self
    }
}

macro_rules! impl_idcounter {
    ($t:ty) => {
        impl IdCounter for $t {
            fn increment(self) -> Self {
                self + 1
            }
        }
    };
}

impl_idcounter!(u8);
impl_idcounter!(u16);
impl_idcounter!(u32);
impl_idcounter!(u64);
impl_idcounter!(usize);
