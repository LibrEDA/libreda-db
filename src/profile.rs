// SPDX-FileCopyrightText: 2018-2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Acquire performance metrics of single data-base functions.
//!
//! # Example
//!
//! ```
//! use libreda_db::prelude::*;
//! use libreda_db::profile::{DBPerf, FnName};
//!
//! let mut chip = Chip::new();
//! let mut chip_with_perf = DBPerf::new(&mut chip);
//!
//! // Do some operations.
//! let _cell = chip_with_perf.create_cell("MyCell".into());
//!
//! let stats = chip_with_perf.get_stats(FnName::create_cell);
//!
//! /// Debug-print the statistics.
//! dbg!(stats);
//!
//! assert_eq!(stats.num_calls, 1, "there was exactly one call to the function");
//! assert!(stats.total_time.as_nanos() > 0);
//! ```

use crate::prelude::*;

use num_traits::FromPrimitive;
use std::sync::atomic::AtomicU64;
use std::time::Duration;

/// Wrapper around netlist and layout data structures.
/// Transparently measures time spent in function calls.
///
/// # Types
/// * `T`: Underlying data structure.
#[derive(Default)]
pub struct DBPerf<T> {
    /// Underlying data structure.
    chip: T,
    perf_counters: PerfCounters,
}

struct PerfCounters {
    perf_counters: Vec<(FnName, PerfCounter)>,
}

impl Default for PerfCounters {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> DBPerf<T> {
    /// Wrap the `chip` structure into a performance counter.
    pub fn new(chip: T) -> Self {
        Self {
            chip,
            perf_counters: PerfCounters::new(),
        }
    }

    /// Get a list of counters associated with the API function names.
    pub fn get_stats_all(&self) -> Vec<(FnName, PerfCounterResult)> {
        self.perf_counters
            .perf_counters
            .iter()
            .map(|(fname, counter)| (*fname, counter.atomic_read()))
            .collect()
    }

    /// Get the performance counter values for the given function.
    pub fn get_stats(&self, function_name: FnName) -> PerfCounterResult {
        self.perf_counters.get(function_name).atomic_read()
    }

    /// Retreive the inner datastructure.
    /// This is used mainly when the inner data is owned and not a reference.
    pub fn into_inner(self) -> T {
        self.chip
    }

    /// Print performance statistics.
    pub fn pretty_print(&self, mut write: impl std::fmt::Write) -> Result<(), std::fmt::Error> {
        let mut stats = self.get_stats_all();

        // Sort by time spent.
        stats.sort_by_key(|(_name, stat)| stat.total_time);

        let mut num_unused_functions = 0;
        for (function_name, stat) in stats {
            // Skip unused functions.
            if stat.num_calls == 0 {
                num_unused_functions += 1;
                continue;
            };

            writeln!(
                write,
                "{}: {} calls, total = {:?}, avg = {:?}",
                function_name,
                stat.num_calls,
                stat.total_time,
                stat.avg_time()
            )?;
        }
        writeln!(
            write,
            "Number of unused DB API functions: {}",
            num_unused_functions
        )
    }
}

impl PerfCounters {
    fn new() -> Self {
        Self {
            perf_counters: (0..)
                .map(FnName::from_usize)
                .take_while(|fname| fname.is_some())
                .map(|fname| (fname.unwrap(), Default::default()))
                .collect(),
        }
    }

    /// Get a counter by the function name.
    fn get(&self, function_name: FnName) -> &PerfCounter {
        &self.perf_counters[function_name as usize].1
    }
}

/// Enum of API names.
/// This is used as an index into the array of performance counters.
#[derive(Copy, Clone, PartialEq, Eq, ToPrimitive, FromPrimitive, Debug)]
#[allow(non_camel_case_types, missing_docs)]
pub enum FnName {
    // HierarchyBase
    cell_by_name = 0,
    cell_instance_by_name,
    cell_name,
    cell_instance_name,
    parent_cell,
    template_cell,
    for_each_cell,
    each_cell_vec,
    each_cell,
    for_each_cell_instance,
    each_cell_instance_vec,
    each_cell_instance,
    for_each_cell_dependency,
    each_cell_dependency_vec,
    each_cell_dependency,
    num_cell_dependencies,
    for_each_dependent_cell,
    each_dependent_cell_vec,
    each_dependent_cell,
    num_dependent_cells,
    for_each_cell_reference,
    each_cell_reference_vec,
    each_cell_reference,
    num_cell_references,
    num_child_instances,
    num_cells,
    get_chip_property,
    get_cell_property,
    get_cell_instance_property,

    // HierarchyEdit,
    create_cell,
    remove_cell,
    create_cell_instance,
    remove_cell_instance,
    rename_cell_instance,
    rename_cell,
    set_chip_property,
    set_cell_property,
    set_cell_instance_property,

    // LayoutBase
    dbu,
    each_layer,
    layer_info,
    find_layer,
    layer_by_name,
    bounding_box_per_layer,
    each_shape_id,
    for_each_shape,
    with_shape,
    parent_of_shape,
    get_transform,
    bounding_box,
    shape_geometry,
    shape_layer,
    for_each_shape_recursive,
    get_shape_property,

    // LayoutEdit
    insert_shape,
    set_dbu,
    create_layer,
    create_layer_with_id,
    set_layer_name,
    remove_shape,
    replace_shape,
    set_transform,
    set_shape_property,

    // NetlistEdit
    create_pin,
    remove_pin,
    rename_pin,
    create_net,
    rename_net,
    remove_net,
    connect_pin,
    connect_pin_instance,
    disconnect_pin,
    disconnect_pin_instance,

    // L2NBase
    shapes_of_net,
    shapes_of_pin,
    get_net_of_shape,
    get_pin_of_shape,

    // L2NEdit
    set_pin_of_shape,
    set_net_of_shape,

    // RegionSearch
    each_shape_in_region,
    each_shape_in_region_per_layer,
    each_cell_instance_in_region,
}

impl std::fmt::Display for FnName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Redirect to `Debug` trait.
        write!(f, "{:?}", self)
    }
}

/// Atomic counters for recording statistics on calls of a single function.
#[derive(Debug)]
pub struct PerfCounter {
    /// Number of function calls.
    num_calls: AtomicU64,
    /// Total time spent in this function.
    total_time_ns: AtomicU64,
    /// Shortest measured duration spend in this function.
    min_time_ns: AtomicU64,
    /// Longest measured duration spend in this function.
    max_time_ns: AtomicU64,
}

/// Statistics on calls of a single function.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct PerfCounterResult {
    /// Number of function calls.
    pub num_calls: u64,
    /// Total time spent in this function.
    pub total_time: Duration,
    /// Shortest measured duration spend in this function.
    pub min_time: Duration,
    /// Longest measured duration spend in this function.
    pub max_time: Duration,
}

impl PerfCounterResult {
    /// Compute the average time used by the measured function.
    /// Returns `None` if the number of function calls is zero.
    pub fn avg_time(&self) -> Option<Duration> {
        (self.num_calls > 0).then(|| self.total_time / (self.num_calls as u32))
    }
}

impl PerfCounter {
    /// Atomically read the current state of the counter.
    pub fn atomic_read(&self) -> PerfCounterResult {
        use std::sync::atomic::Ordering::Relaxed;

        loop {
            let read = || PerfCounterResult {
                num_calls: self.num_calls.load(Relaxed),
                total_time: Duration::from_nanos(self.total_time_ns.load(Relaxed)),
                min_time: Duration::from_nanos(self.min_time_ns.load(Relaxed)),
                max_time: Duration::from_nanos(self.max_time_ns.load(Relaxed)),
            };

            let r = read();
            if r == read() {
                // There was no write to the counters in the meantime.
                break r;
            }

            std::hint::spin_loop();
        }
    }
}

impl Default for PerfCounter {
    fn default() -> Self {
        Self {
            num_calls: Default::default(),
            total_time_ns: Default::default(),
            min_time_ns: AtomicU64::new(u64::MAX),
            max_time_ns: Default::default(),
        }
    }
}

/// Context manager for a performance counter.
/// This is used to track when the measured function exits.
pub struct PerfCounterManager<'a> {
    /// Instant then the time measurement was started.
    start_time: std::time::Instant,
    counter: &'a PerfCounter,
}

impl PerfCounterManager<'_> {
    /// Stop measuring the time and add the duration to the total spent time.
    fn stop_measurement(self) {
        let elapsed = self.start_time.elapsed();
        let elapsed_ns = elapsed.as_nanos() as u64;
        use std::sync::atomic::Ordering::*;
        self.counter.total_time_ns.fetch_add(elapsed_ns, Relaxed);
        self.counter.min_time_ns.fetch_min(elapsed_ns, Relaxed);
        self.counter.max_time_ns.fetch_max(elapsed_ns, Relaxed);
        self.counter.num_calls.fetch_add(1, Relaxed);
    }
}

impl PerfCounter {
    /// Start measuring the spent time.
    /// Must call `stop()` on the return value.
    #[must_use]
    fn start_measurement(&self) -> PerfCounterManager {
        PerfCounterManager {
            start_time: std::time::Instant::now(),
            counter: self,
        }
    }

    /// Measure the execution time of `f` and add it to the statistics.
    fn measure<R>(&self, f: impl FnOnce() -> R) -> R {
        let m = self.start_measurement();
        let r = f();
        m.stop_measurement();
        r
    }
}

#[portrait::fill(portrait::delegate(H))]
impl<H: HierarchyIds> HierarchyIds for DBPerf<H> {}

#[portrait::fill(portrait::delegate(N))]
impl<N: NetlistIds> NetlistIds for DBPerf<N> {}

// Inherit everything from HierarchyBase.
impl<H: HierarchyBase> HierarchyBase for DBPerf<H> {
    type NameType = H::NameType;

    fn cell_by_name(&self, name: &str) -> Option<H::CellId> {
        self.perf_counters
            .get(FnName::cell_by_name)
            .measure(|| self.chip.cell_by_name(name))
    }

    fn cell_instance_by_name(&self, parent_cell: &H::CellId, name: &str) -> Option<H::CellInstId> {
        self.perf_counters
            .get(FnName::cell_instance_by_name)
            .measure(|| self.chip.cell_instance_by_name(parent_cell, name))
    }

    fn cell_name(&self, cell: &H::CellId) -> H::NameType {
        self.perf_counters
            .get(FnName::cell_name)
            .measure(|| self.chip.cell_name(cell))
    }

    fn cell_instance_name(&self, cell_inst: &H::CellInstId) -> Option<H::NameType> {
        self.perf_counters
            .get(FnName::cell_instance_name)
            .measure(|| self.chip.cell_instance_name(cell_inst))
    }

    fn parent_cell(&self, cell_instance: &H::CellInstId) -> H::CellId {
        self.perf_counters
            .get(FnName::parent_cell)
            .measure(|| self.chip.parent_cell(cell_instance))
    }

    fn template_cell(&self, cell_instance: &H::CellInstId) -> H::CellId {
        self.perf_counters
            .get(FnName::template_cell)
            .measure(|| self.chip.template_cell(cell_instance))
    }

    fn for_each_cell<F>(&self, f: F)
    where
        F: FnMut(H::CellId),
    {
        self.perf_counters
            .get(FnName::for_each_cell)
            .measure(|| self.chip.for_each_cell(f))
    }

    fn each_cell_vec(&self) -> Vec<H::CellId> {
        self.perf_counters
            .get(FnName::each_cell_vec)
            .measure(|| self.chip.each_cell_vec())
    }

    fn each_cell(&self) -> Box<dyn Iterator<Item = H::CellId> + '_> {
        self.perf_counters
            .get(FnName::each_cell)
            .measure(|| self.chip.each_cell())
    }

    fn for_each_cell_instance<F>(&self, cell: &H::CellId, f: F)
    where
        F: FnMut(H::CellInstId),
    {
        self.perf_counters
            .get(FnName::each_cell_instance)
            .measure(|| self.chip.for_each_cell_instance(cell, f))
    }

    fn each_cell_instance_vec(&self, cell: &H::CellId) -> Vec<H::CellInstId> {
        self.perf_counters
            .get(FnName::each_cell_instance_vec)
            .measure(|| self.chip.each_cell_instance_vec(cell))
    }

    fn each_cell_instance(&self, cell: &H::CellId) -> Box<dyn Iterator<Item = H::CellInstId> + '_> {
        self.perf_counters
            .get(FnName::each_cell_instance)
            .measure(|| self.chip.each_cell_instance(cell))
    }

    fn for_each_cell_dependency<F>(&self, cell: &H::CellId, f: F)
    where
        F: FnMut(H::CellId),
    {
        self.perf_counters
            .get(FnName::for_each_cell_dependency)
            .measure(|| self.chip.for_each_cell_dependency(cell, f))
    }

    fn each_cell_dependency_vec(&self, cell: &H::CellId) -> Vec<H::CellId> {
        self.perf_counters
            .get(FnName::each_cell_dependency_vec)
            .measure(|| self.chip.each_cell_dependency_vec(cell))
    }

    fn each_cell_dependency(&self, cell: &H::CellId) -> Box<dyn Iterator<Item = H::CellId> + '_> {
        self.perf_counters
            .get(FnName::each_cell_dependency)
            .measure(|| self.chip.each_cell_dependency(cell))
    }

    fn num_cell_dependencies(&self, cell: &H::CellId) -> usize {
        self.perf_counters
            .get(FnName::num_cell_dependencies)
            .measure(|| self.chip.num_cell_dependencies(cell))
    }

    fn for_each_dependent_cell<F>(&self, cell: &H::CellId, f: F)
    where
        F: FnMut(H::CellId),
    {
        self.perf_counters
            .get(FnName::for_each_dependent_cell)
            .measure(|| self.chip.for_each_dependent_cell(cell, f))
    }

    fn each_dependent_cell_vec(&self, cell: &H::CellId) -> Vec<H::CellId> {
        self.perf_counters
            .get(FnName::each_dependent_cell_vec)
            .measure(|| self.chip.each_dependent_cell_vec(cell))
    }

    fn each_dependent_cell(&self, cell: &H::CellId) -> Box<dyn Iterator<Item = H::CellId> + '_> {
        self.perf_counters
            .get(FnName::each_dependent_cell)
            .measure(|| self.chip.each_dependent_cell(cell))
    }

    fn num_dependent_cells(&self, cell: &H::CellId) -> usize {
        self.perf_counters
            .get(FnName::num_dependent_cells)
            .measure(|| self.chip.num_dependent_cells(cell))
    }

    fn for_each_cell_reference<F>(&self, cell: &H::CellId, f: F)
    where
        F: FnMut(H::CellInstId),
    {
        self.perf_counters
            .get(FnName::for_each_cell_reference)
            .measure(|| self.chip.for_each_cell_reference(cell, f))
    }

    fn each_cell_reference_vec(&self, cell: &H::CellId) -> Vec<H::CellInstId> {
        self.perf_counters
            .get(FnName::each_cell_reference_vec)
            .measure(|| self.chip.each_cell_reference_vec(cell))
    }

    fn each_cell_reference(
        &self,
        cell: &H::CellId,
    ) -> Box<dyn Iterator<Item = H::CellInstId> + '_> {
        self.perf_counters
            .get(FnName::each_cell_reference)
            .measure(|| self.chip.each_cell_reference(cell))
    }

    fn num_cell_references(&self, cell: &H::CellId) -> usize {
        self.perf_counters
            .get(FnName::num_cell_references)
            .measure(|| self.chip.num_cell_references(cell))
    }

    fn num_child_instances(&self, cell: &H::CellId) -> usize {
        self.perf_counters
            .get(FnName::num_child_instances)
            .measure(|| self.chip.num_child_instances(cell))
    }

    fn num_cells(&self) -> usize {
        self.perf_counters
            .get(FnName::num_cells)
            .measure(|| self.chip.num_cells())
    }

    fn get_chip_property(&self, key: &H::NameType) -> Option<PropertyValue> {
        self.perf_counters
            .get(FnName::get_chip_property)
            .measure(|| self.chip.get_chip_property(key))
    }

    fn get_cell_property(&self, cell: &H::CellId, key: &H::NameType) -> Option<PropertyValue> {
        self.perf_counters
            .get(FnName::get_cell_property)
            .measure(|| self.chip.get_cell_property(cell, key))
    }

    fn get_cell_instance_property(
        &self,
        inst: &H::CellInstId,
        key: &H::NameType,
    ) -> Option<PropertyValue> {
        self.perf_counters
            .get(FnName::get_cell_instance_property)
            .measure(|| self.chip.get_cell_instance_property(inst, key))
    }
}

// Inherit everything from LayoutBase.
#[portrait::fill(portrait::delegate(L))]
impl<L: LayoutIds> LayoutIds for DBPerf<L> {}

// Inherit everything from LayoutBase.
// #[portrait::fill(portrait::delegate(L; self.chip))]
impl<L: LayoutBase> LayoutBase for DBPerf<L> {
    fn dbu(&self) -> Self::Coord {
        self.perf_counters
            .get(FnName::dbu)
            .measure(|| self.chip.dbu())
    }

    fn each_layer(&self) -> Box<dyn Iterator<Item = Self::LayerId> + '_> {
        self.perf_counters
            .get(FnName::each_layer)
            .measure(|| self.chip.each_layer())
    }

    fn layer_info(&self, layer: &Self::LayerId) -> LayerInfo<Self::NameType> {
        self.perf_counters
            .get(FnName::layer_info)
            .measure(|| self.chip.layer_info(layer))
    }

    fn find_layer(&self, index: UInt, datatype: UInt) -> Option<Self::LayerId> {
        self.perf_counters
            .get(FnName::find_layer)
            .measure(|| self.chip.find_layer(index, datatype))
    }

    fn layer_by_name(&self, name: &str) -> Option<Self::LayerId> {
        self.perf_counters
            .get(FnName::layer_by_name)
            .measure(|| self.chip.layer_by_name(name))
    }

    fn bounding_box_per_layer(
        &self,
        cell: &Self::CellId,
        layer: &Self::LayerId,
    ) -> Option<Rect<Self::Coord>> {
        self.perf_counters
            .get(FnName::bounding_box_per_layer)
            .measure(|| self.chip.bounding_box_per_layer(cell, layer))
    }

    fn each_shape_id(
        &self,
        cell: &Self::CellId,
        layer: &Self::LayerId,
    ) -> Box<dyn Iterator<Item = Self::ShapeId> + '_> {
        self.perf_counters
            .get(FnName::each_shape_id)
            .measure(|| self.chip.each_shape_id(cell, layer))
    }

    fn for_each_shape<F>(&self, cell: &Self::CellId, layer: &Self::LayerId, f: F)
    where
        F: FnMut(&Self::ShapeId, &Geometry<Self::Coord>),
    {
        self.perf_counters
            .get(FnName::for_each_shape)
            .measure(|| self.chip.for_each_shape(cell, layer, f))
    }

    fn with_shape<F, R>(&self, shape_id: &Self::ShapeId, f: F) -> R
    where
        F: FnMut(&Self::LayerId, &Geometry<Self::Coord>) -> R,
    {
        self.perf_counters
            .get(FnName::with_shape)
            .measure(|| self.chip.with_shape(shape_id, f))
    }

    fn parent_of_shape(&self, shape_id: &Self::ShapeId) -> (Self::CellId, Self::LayerId) {
        self.perf_counters
            .get(FnName::parent_of_shape)
            .measure(|| self.chip.parent_of_shape(shape_id))
    }

    fn get_transform(&self, cell_inst: &Self::CellInstId) -> SimpleTransform<Self::Coord> {
        self.perf_counters
            .get(FnName::get_transform)
            .measure(|| self.chip.get_transform(cell_inst))
    }

    fn bounding_box(&self, cell: &Self::CellId) -> Option<Rect<Self::Coord>> {
        self.perf_counters
            .get(FnName::bounding_box)
            .measure(|| self.chip.bounding_box(cell))
    }

    fn shape_geometry(&self, shape_id: &Self::ShapeId) -> Geometry<Self::Coord> {
        self.perf_counters
            .get(FnName::shape_geometry)
            .measure(|| self.chip.shape_geometry(shape_id))
    }

    fn shape_layer(&self, shape_id: &Self::ShapeId) -> Self::LayerId {
        self.perf_counters
            .get(FnName::shape_layer)
            .measure(|| self.chip.shape_layer(shape_id))
    }

    fn for_each_shape_recursive<F>(&self, cell: &Self::CellId, layer: &Self::LayerId, f: F)
    where
        F: FnMut(SimpleTransform<Self::Coord>, &Self::ShapeId, &Geometry<Self::Coord>),
    {
        self.perf_counters
            .get(FnName::for_each_shape_recursive)
            .measure(|| self.chip.for_each_shape_recursive(cell, layer, f))
    }

    fn get_shape_property(
        &self,
        shape: &Self::ShapeId,
        key: &Self::NameType,
    ) -> Option<PropertyValue> {
        self.perf_counters
            .get(FnName::get_shape_property)
            .measure(|| self.chip.get_shape_property(shape, key))
    }
}

// Inherit everything from NetlistBase.
#[portrait::fill(portrait::delegate(N; self.chip))]
impl<N: NetlistBase> NetlistBase for DBPerf<N> {}

// Inherit everything from HierarchyEdit.
impl<H: HierarchyEdit> HierarchyEdit for DBPerf<H> {
    fn create_cell(&mut self, name: H::NameType) -> H::CellId {
        self.perf_counters
            .get(FnName::create_cell)
            .measure(|| self.chip.create_cell(name))
    }

    fn remove_cell(&mut self, cell_id: &H::CellId) {
        self.perf_counters
            .get(FnName::remove_cell)
            .measure(|| self.chip.remove_cell(cell_id))
    }

    fn create_cell_instance(
        &mut self,
        parent_cell: &H::CellId,
        template_cell: &H::CellId,
        name: Option<H::NameType>,
    ) -> H::CellInstId {
        self.perf_counters
            .get(FnName::create_cell_instance)
            .measure(|| {
                self.chip
                    .create_cell_instance(parent_cell, template_cell, name)
            })
    }

    fn remove_cell_instance(&mut self, inst: &H::CellInstId) {
        self.perf_counters
            .get(FnName::remove_cell_instance)
            .measure(|| self.chip.remove_cell_instance(inst))
    }

    fn rename_cell_instance(&mut self, inst: &H::CellInstId, new_name: Option<H::NameType>) {
        self.perf_counters
            .get(FnName::rename_cell_instance)
            .measure(|| self.chip.rename_cell_instance(inst, new_name))
    }

    fn rename_cell(&mut self, cell: &H::CellId, new_name: H::NameType) {
        self.perf_counters
            .get(FnName::rename_cell)
            .measure(|| self.chip.rename_cell(cell, new_name))
    }

    fn set_chip_property(&mut self, key: H::NameType, value: PropertyValue) {
        self.perf_counters
            .get(FnName::set_chip_property)
            .measure(|| self.chip.set_chip_property(key, value))
    }

    fn set_cell_property(&mut self, cell: &H::CellId, key: H::NameType, value: PropertyValue) {
        self.perf_counters
            .get(FnName::set_cell_property)
            .measure(|| self.chip.set_cell_property(cell, key, value))
    }

    fn set_cell_instance_property(
        &mut self,
        inst: &H::CellInstId,
        key: H::NameType,
        value: PropertyValue,
    ) {
        self.perf_counters
            .get(FnName::set_cell_instance_property)
            .measure(|| self.chip.set_cell_instance_property(inst, key, value))
    }
}

// Inherit everything from LayoutEdit.
impl<L: LayoutEdit> LayoutEdit for DBPerf<L> {
    fn insert_shape(
        &mut self,
        parent_cell: &L::CellId,
        layer: &L::LayerId,
        geometry: Geometry<L::Coord>,
    ) -> L::ShapeId {
        self.perf_counters
            .get(FnName::insert_shape)
            .measure(|| self.chip.insert_shape(parent_cell, layer, geometry))
    }

    fn set_dbu(&mut self, dbu: L::Coord) {
        self.perf_counters
            .get(FnName::set_dbu)
            .measure(|| self.chip.set_dbu(dbu))
    }

    fn create_layer(&mut self, index: UInt, datatype: UInt) -> L::LayerId {
        self.perf_counters
            .get(FnName::create_layer)
            .measure(|| self.chip.create_layer(index, datatype))
    }

    fn create_layer_with_id(
        &mut self,
        layer_id: L::LayerId,
        index: UInt,
        datatype: UInt,
    ) -> Result<(), ()> {
        self.perf_counters
            .get(FnName::create_layer_with_id)
            .measure(|| self.chip.create_layer_with_id(layer_id, index, datatype))
    }

    fn set_layer_name(
        &mut self,
        layer: &L::LayerId,
        name: Option<L::NameType>,
    ) -> Option<L::NameType> {
        self.perf_counters
            .get(FnName::set_layer_name)
            .measure(|| self.chip.set_layer_name(layer, name))
    }

    fn remove_shape(&mut self, shape_id: &L::ShapeId) -> Option<Geometry<L::Coord>> {
        self.perf_counters
            .get(FnName::remove_shape)
            .measure(|| self.chip.remove_shape(shape_id))
    }

    fn replace_shape(
        &mut self,
        shape_id: &L::ShapeId,
        geometry: Geometry<L::Coord>,
    ) -> Geometry<L::Coord> {
        self.perf_counters
            .get(FnName::replace_shape)
            .measure(|| self.chip.replace_shape(shape_id, geometry))
    }

    fn set_transform(&mut self, cell_inst: &L::CellInstId, tf: SimpleTransform<L::Coord>) {
        self.perf_counters
            .get(FnName::set_transform)
            .measure(|| self.chip.set_transform(cell_inst, tf))
    }

    fn set_shape_property(&mut self, shape: &L::ShapeId, key: L::NameType, value: PropertyValue) {
        self.perf_counters
            .get(FnName::set_shape_property)
            .measure(|| self.chip.set_shape_property(shape, key, value))
    }
}

impl<N: NetlistEdit> NetlistEdit for DBPerf<N> {
    fn create_pin(
        &mut self,
        cell: &Self::CellId,
        name: Self::NameType,
        direction: Direction,
    ) -> Self::PinId {
        self.perf_counters
            .get(FnName::create_pin)
            .measure(|| self.chip.create_pin(cell, name, direction))
    }

    fn remove_pin(&mut self, id: &Self::PinId) {
        self.perf_counters
            .get(FnName::remove_pin)
            .measure(|| self.chip.remove_pin(id))
    }

    fn rename_pin(&mut self, pin: &Self::PinId, new_name: Self::NameType) -> Self::NameType {
        self.perf_counters
            .get(FnName::rename_pin)
            .measure(|| self.chip.rename_pin(pin, new_name))
    }

    fn create_net(&mut self, parent: &Self::CellId, name: Option<Self::NameType>) -> Self::NetId {
        self.perf_counters
            .get(FnName::create_net)
            .measure(|| self.chip.create_net(parent, name))
    }

    fn rename_net(
        &mut self,
        net_id: &Self::NetId,
        new_name: Option<Self::NameType>,
    ) -> Option<Self::NameType> {
        self.perf_counters
            .get(FnName::rename_net)
            .measure(|| self.chip.rename_net(net_id, new_name))
    }

    fn remove_net(&mut self, net: &Self::NetId) {
        self.perf_counters
            .get(FnName::remove_net)
            .measure(|| self.chip.remove_net(net))
    }

    fn connect_pin(&mut self, pin: &Self::PinId, net: Option<Self::NetId>) -> Option<Self::NetId> {
        self.perf_counters
            .get(FnName::connect_pin)
            .measure(|| self.chip.connect_pin(pin, net))
    }

    fn connect_pin_instance(
        &mut self,
        pin: &Self::PinInstId,
        net: Option<Self::NetId>,
    ) -> Option<Self::NetId> {
        self.perf_counters
            .get(FnName::connect_pin_instance)
            .measure(|| self.chip.connect_pin_instance(pin, net))
    }

    fn disconnect_pin(&mut self, pin: &Self::PinId) -> Option<Self::NetId> {
        self.perf_counters
            .get(FnName::disconnect_pin)
            .measure(|| self.chip.disconnect_pin(pin))
    }

    fn disconnect_pin_instance(&mut self, pin_instance: &Self::PinInstId) -> Option<Self::NetId> {
        self.perf_counters
            .get(FnName::disconnect_pin_instance)
            .measure(|| self.chip.disconnect_pin_instance(pin_instance))
    }
}

impl<L: L2NBase> L2NBase for DBPerf<L> {
    fn shapes_of_net(&self, net_id: &Self::NetId) -> Box<dyn Iterator<Item = Self::ShapeId> + '_> {
        self.perf_counters
            .get(FnName::shapes_of_net)
            .measure(|| self.chip.shapes_of_net(net_id))
    }

    fn shapes_of_pin(&self, pin_id: &Self::PinId) -> Box<dyn Iterator<Item = Self::ShapeId> + '_> {
        self.perf_counters
            .get(FnName::shapes_of_pin)
            .measure(|| self.chip.shapes_of_pin(pin_id))
    }

    fn get_net_of_shape(&self, shape_id: &Self::ShapeId) -> Option<Self::NetId> {
        self.perf_counters
            .get(FnName::get_net_of_shape)
            .measure(|| self.chip.get_net_of_shape(shape_id))
    }

    fn get_pin_of_shape(&self, shape_id: &Self::ShapeId) -> Option<Self::PinId> {
        self.perf_counters
            .get(FnName::get_pin_of_shape)
            .measure(|| self.chip.get_pin_of_shape(shape_id))
    }
}

impl<L: L2NEdit> L2NEdit for DBPerf<L> {
    fn set_pin_of_shape(
        &mut self,
        shape_id: &Self::ShapeId,
        pin: Option<Self::PinId>,
    ) -> Option<Self::PinId> {
        self.perf_counters
            .get(FnName::set_pin_of_shape)
            .measure(|| self.chip.set_pin_of_shape(shape_id, pin))
    }

    fn set_net_of_shape(
        &mut self,
        shape_id: &Self::ShapeId,
        net: Option<Self::NetId>,
    ) -> Option<Self::NetId> {
        self.perf_counters
            .get(FnName::set_net_of_shape)
            .measure(|| self.chip.set_net_of_shape(shape_id, net))
    }
}

impl<L: RegionSearch> RegionSearch for DBPerf<L> {
    fn each_shape_in_region(
        &self,
        cell: &Self::CellId,
        search_region: &Rect<Self::Coord>,
    ) -> Box<dyn Iterator<Item = Self::ShapeId> + '_> {
        self.perf_counters
            .get(FnName::each_shape_in_region)
            .measure(|| self.chip.each_shape_in_region(cell, search_region))
    }

    fn each_shape_in_region_per_layer(
        &self,
        cell: &Self::CellId,
        layer_id: &Self::LayerId,
        search_region: &Rect<Self::Coord>,
    ) -> Box<dyn Iterator<Item = Self::ShapeId> + '_> {
        self.perf_counters
            .get(FnName::each_shape_in_region_per_layer)
            .measure(|| {
                self.chip
                    .each_shape_in_region_per_layer(cell, layer_id, search_region)
            })
    }

    fn each_cell_instance_in_region(
        &self,
        cell: &Self::CellId,
        search_region: &Rect<Self::Coord>,
    ) -> Box<dyn Iterator<Item = Self::CellInstId> + '_> {
        self.perf_counters
            .get(FnName::each_cell_instance_in_region)
            .measure(|| self.chip.each_cell_instance_in_region(cell, search_region))
    }
}

#[test]
fn test_dbperf() {
    use crate::chip::Chip;
    let mut chip = Chip::new();
    let mut chip_perf = DBPerf::new(&mut chip);

    let _cell = chip_perf.create_cell("A".into());
    let _cell = chip_perf.create_cell("B".into());
    let _cell = chip_perf.create_cell("C".into());

    // Read performance stats of the 'create_cell' function.
    let create_cell_perf = chip_perf.get_stats(FnName::create_cell);
    dbg!(create_cell_perf);

    assert_eq!(create_cell_perf.num_calls, 3);
    assert!(!create_cell_perf.total_time.is_zero());
    assert!(create_cell_perf.min_time <= create_cell_perf.max_time);
    assert!(create_cell_perf.min_time <= create_cell_perf.avg_time().unwrap());
    assert!(create_cell_perf.avg_time().unwrap() <= create_cell_perf.max_time);
}
