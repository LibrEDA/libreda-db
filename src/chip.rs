// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Chip data structure holding netlist and layout together.
//! [`Chip`] implements the [`trait@L2NEdit`] trait and hence qualifies for representing
//! netlists fused with a layout. [`Chip`] can also be used solely as a netlist structure
//! or as a layout structure.

use iron_shapes::prelude::{Geometry, Rect};
use iron_shapes::transform::SimpleTransform;
use iron_shapes::CoordinateType;

use smallvec::{smallvec, SmallVec};

use crate::prelude::{
    HierarchyBase, HierarchyEdit, HierarchyIds, L2NBase, L2NEdit, LayoutBase, LayoutEdit,
    MapPointwise, NetlistBase, NetlistEdit,
};
use crate::slab_alloc::{SlabAlloc, SlabIndex};
use crate::traits::{LayoutIds, NetlistIds};
use itertools::Itertools;
use std::borrow::Borrow;
use std::collections::HashMap;
use std::hash::Hash;

// use crate::rc_string::RcString;
use std::fmt::Debug;

use crate::property_storage::PropertyStore;
use libreda_core::prelude::{Direction, LayerInfo, PropertyValue};

// Use an alternative hasher that has better performance for integer keys.
use fnv::{FnvHashMap, FnvHashSet};

use crate::prelude::TryBoundingBox;
use num_traits::One;

type NameT = String;

type IntHashMap<K, V> = FnvHashMap<K, V>;
type IntHashSet<V> = FnvHashSet<V>;

/// Default unsigned integer type.
pub type UInt = u32;
/// Default signed integer type.
pub type SInt = i32;

/// Integer coordinate type.
pub type Coord = i32;
/// Integer area type.
pub type Area = i64;

/// Type used for detecting reuse of previously freed indices
/// in a slab allocator.
/// Could be set to `()` for space efficiency. But then certain bugs would
/// not get catched immediately.
type SlabId = u8; // `ZeroBitCounter` uses less memory and is faster but comes at the risk of accepting obsolete IDs

/// Cell identifier.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct CellId(SlabIndex<u16, SlabId>);

/// Cell instance identifier.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct CellInstId(CellId, SlabIndex<u32, SlabId>);

/// Pin identifier.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct PinId(CellId, SlabIndex<u16, SlabId>);

/// Pin instance identifier.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct PinInstId(CellInstId, SlabIndex<u16, SlabId>);

/// Either a pin or pin instance identifier.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum TerminalId {
    /// Terminal is a pin.
    Pin(PinId),
    /// Terminal is a pin instance.
    PinInst(PinInstId),
}

impl From<PinId> for TerminalId {
    fn from(id: PinId) -> Self {
        TerminalId::Pin(id)
    }
}

impl From<PinInstId> for TerminalId {
    fn from(id: PinInstId) -> Self {
        TerminalId::PinInst(id)
    }
}

/// Net identifier.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct NetId(CellId, SlabIndex<u32, SlabId>);

/// Unique (across layout) identifier of a shape.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct ShapeId(CellId, LayerId, SlabIndex<u32, SlabId>);

/// ID for layers.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct LayerId(SlabIndex<u16, SlabId>);

/// A cell is defined by an interface (pins) and
/// a content which consists of interconnected cell instances.
///
/// Template parameters:
///
/// * `U`: User defined data.
#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
struct Cell<C = Coord, U = ()>
where
    C: CoordinateType,
    U: Default,
{
    /// Name of the cell.
    name: NameT,

    /// Instances inside this cell.
    instances: SlabAlloc<CellInst, u32, SlabId>,
    /// Instances inside this cell indexed by name.
    /// Not every instance needs to have a name.
    instances_by_name: HashMap<NameT, CellInstId>,
    /// Cell instances that reference to this cell.
    references: SlabAlloc<CellInstId, u32>,

    /// Set of cells that are direct dependencies of this cell.
    /// Stored together with a counter of how many instances of the dependency are present.
    /// This are the cells towards the leaves in the dependency tree.
    dependencies: IntHashMap<CellId, usize>,
    /// Cells that use a instance of this cell.
    dependent_cells: IntHashMap<CellId, usize>,

    /// Properties related to the instances in this template.
    /// Instance properties are stored here for lower overhead of cell instances.
    #[allow(unused)]
    instance_properties: IntHashMap<CellInstId, PropertyStore<NameT>>,
    /// Properties related to this template.
    properties: PropertyStore<NameT>,
    /// User-defined data.
    #[allow(unused)]
    user_data: U,

    // == Netlist == //
    pins_ordered: Vec<PinId>,
    pins: SlabAlloc<Pin, u16, SlabId>,
    /// All nets in this cell.
    nets: SlabAlloc<Net, u32, SlabId>,
    /// Nets IDs stored by name.
    nets_by_name: HashMap<NameT, NetId>,
    /// Logic constant LOW net.
    net_low: NetId,
    /// Logic constant HIGH net.
    net_high: NetId,

    // == Layout == //
    /// Mapping from layer indices to geometry data.
    shapes_map: Vec<Shapes<C>>,
}

impl Cell {
    // == Layout == //

    /// Get the shape container of this layer.
    /// Returns `None` if the shapes object does not exist for this layer.
    fn shapes(&self, layer_id: &LayerId) -> Option<&Shapes<Coord>> {
        // TODO: remove
        self.shapes_map.get(layer_id.0.index())
    }

    /// Get the mutable shape container of this layer.
    /// Returns `None` if the shapes object does not exist for this layer.
    fn shapes_mut(&mut self, layer_id: &LayerId) -> Option<&mut Shapes<Coord>> {
        // TODO: remove
        self.shapes_map.get_mut(layer_id.0.index())
    }

    /// Get a mutable reference to the shapes container of the `layer`. When none exists
    /// a shapes object is created for this layer.
    fn get_or_create_shapes_mut(&mut self, layer_id: &LayerId) -> &mut Shapes<Coord> {
        let idx = layer_id.0.index();
        if self.shapes_map.len() <= idx {
            self.shapes_map.resize_with(idx + 1, Default::default)
        }
        &mut self.shapes_map[idx]
    }
}

// pub enum PlacementStatus {
//     Unplaced,
//     Fixed
// }

/// Instance of a cell.
///
/// Template parameters:
///
/// * `U`: User defined data.
#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
struct CellInst<C = Coord, U = ()>
where
    C: CoordinateType,
{
    /// Name of the instance.
    name: Option<NameT>,
    /// The ID of the template cell.
    template_cell_id: CellId,
    /// The ID of the parent cell where this instance lives in.
    parent_cell_id: CellId,
    /// Properties related to this instance.
    properties: PropertyStore<NameT>, // TODO: Move this to `Cell` to avoid memory allocation for each instance.

    /// Index into the list of references which is stored
    /// in the template cell.
    /// Used to remove the link to this instance from the template when
    /// deleting this instance.
    reference_id: SlabIndex<u32>,

    /// User-defined data.
    #[allow(unused)]
    user_data: U,

    // == Netlist == //
    pin_instances: SlabAlloc<PinInst, u16, SlabId>, // TODO: avoid memory allocation for each cell instance
    /// Ordered list of pins of this instance.
    pins_ordered: Vec<PinInstId>, // TODO: avoid memory allocation for each cell instance

    // == Layout == //
    /// Transformation to put the cell to the right place an into the right scale/rotation.
    transform: SimpleTransform<C>,
    // TODO: Repetition
    // /// Current status of the cell placement.
    // placement_status: PlacementStatus
}

impl CellInst {
    // == Layout == //

    /// Get the transformation that represents the location and orientation of this instance.
    fn get_transform(&self) -> &SimpleTransform<Coord> {
        &self.transform
    }

    /// Set the transformation that represents the location and orientation of this instance.
    fn set_transform(&mut self, tf: SimpleTransform<Coord>) {
        self.transform = tf;
    }
}

/// Single bit wire pin.
#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
struct Pin {
    /// Name of the pin.
    name: NameT,
    /// Signal type/direction of the pin.
    direction: Direction,
    /// Parent cell of this pin.
    cell: CellId,
    /// Net that is connected to this pin.
    net: Option<NetId>,

    // == Layout == //
    /// List of shapes in the layout that represent the physical pin.
    pin_shapes: IntHashSet<ShapeId>,
}

/// Instance of a pin.
#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
struct PinInst {
    /// ID of the template pin.
    pub template_pin_id: PinId,
    /// Cell instance where this pin instance lives in.
    pub cell_inst: CellInstId,
    /// Net connected to this pin instance.
    net: Option<NetId>,
}

/// A net represents an electric potential or a wire.
#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
struct Net {
    /// Name of the net.
    pub name: Option<NameT>,
    /// Pins connected to this net.
    pub pins: IntHashSet<PinId>,
    /// Pin instances connected to this net.
    pub pin_instances: IntHashSet<PinInstId>,

    // == Layout == //
    /// List of shapes in the layout that represent the physical net.
    pub net_shapes: IntHashSet<ShapeId>,
}

impl Net {}

/// A netlist is the container of cells.
#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Chip<C: CoordinateType = Coord> {
    cells: SlabAlloc<Cell<C>, u16, SlabId>,
    cells_by_name: HashMap<NameT, CellId>,

    /// Top-level properties.
    properties: PropertyStore<NameT>,

    // == Layout == //
    dbu: C,

    /// Lookup table for finding layers by name.
    layers_by_name: HashMap<NameT, LayerId>,
    /// Lookup table for finding layers by index/datatype numbers.
    layers_by_index_datatype: IntHashMap<(UInt, UInt), LayerId>,
    /// Info structures for all layers.
    layer_info: SlabAlloc<LayerInfo<NameT>, u16, SlabId>,
}

impl<C: CoordinateType + One> Default for Chip<C> {
    fn default() -> Self {
        Self {
            cells: Default::default(),
            cells_by_name: Default::default(),
            properties: Default::default(),
            dbu: C::one(),
            layers_by_name: Default::default(),
            layers_by_index_datatype: Default::default(),
            layer_info: Default::default(),
        }
    }
}

impl Chip<Coord> {
    /// Create an empty data structure.
    pub fn new() -> Self {
        Chip::default()
    }

    /// Find a cell by its name.
    fn cell_by_name<S: ?Sized + Eq + Hash>(&self, name: &S) -> Option<CellId>
    where
        NameT: Borrow<S>,
    {
        self.cells_by_name.get(name).copied()
    }

    /// Change the name of the cell.
    ///
    /// # Panics
    /// Panics if the name already exists.
    fn rename_cell(&mut self, cell: &CellId, name: NameT) {
        assert!(
            !self.cells_by_name.contains_key(&name),
            "Cell with this name already exists: {}",
            &name
        );

        // Remove old name.
        let old_name = &self.cells[cell.0].name;
        let id = self.cells_by_name.remove(old_name);
        debug_assert_eq!(id.as_ref(), Some(cell));

        // Set the new name.
        self.cell_mut(cell).name.clone_from(&name);
        self.cells_by_name.insert(name, *cell);
    }

    /// Change the name of the cell instance.
    ///
    /// # Panics
    /// Panics if the name already exists.
    fn rename_cell_instance(&mut self, inst: &CellInstId, name: Option<NameT>) {
        let parent = self.parent_cell(inst);
        if let Some(name) = &name {
            assert!(
                !self.cell(&parent).instances_by_name.contains_key(name),
                "Cell with this name already exists: {}",
                name
            );
        }

        // Remove old name.
        let old_name = self.cell_inst_mut(inst).name.take();
        if let Some(old_name) = old_name {
            self.cell_mut(&parent).instances_by_name.remove(&old_name);
        }

        self.cell_inst_mut(inst).name.clone_from(&name);
        if let Some(name) = name {
            self.cell_mut(&parent).instances_by_name.insert(name, *inst);
        }
    }

    /// Create a new cell template.
    fn create_cell(&mut self, name: NameT, pins: Vec<(NameT, Direction)>) -> CellId {
        assert!(
            !self.cells_by_name.contains_key(&name),
            "Cell with this name already exists: {}",
            &name
        );

        let cell = Cell {
            name: name.clone(),
            pins: Default::default(),
            pins_ordered: Default::default(),
            instances: Default::default(),
            instances_by_name: Default::default(),
            references: Default::default(),
            nets: Default::default(),
            nets_by_name: Default::default(),
            // Create LOW and HIGH nets.
            net_low: NetId(CellId(Default::default()), Default::default()),
            net_high: NetId(CellId(Default::default()), Default::default()),
            dependent_cells: Default::default(),
            instance_properties: Default::default(),
            dependencies: Default::default(),
            user_data: Default::default(),
            shapes_map: Default::default(),
            properties: Default::default(),
        };

        let id = CellId(self.cells.insert(cell));
        self.cells_by_name.insert(name, id);

        // Create LOW and HIGH nets.
        let net_low = self.create_net(&id, Some("__LOW__".into()));
        let net_high = self.create_net(&id, Some("__HIGH__".into()));

        let c = self.cell_mut(&id);
        c.net_low = net_low;
        c.net_high = net_high;

        // Create pins.
        pins.into_iter().for_each(|(name, direction)| {
            self.create_pin(id, name, direction);
        });

        id
    }

    /// Remove all instances inside the cell, remove all instances of the cell
    /// and remove finally the cell itself.
    fn remove_cell(&mut self, cell_id: &CellId) {
        // Remove all instances inside this cell.
        let instances = self
            .cell(cell_id)
            .instances
            .iter()
            .map(|(id, _inst)| CellInstId(*cell_id, id))
            .collect_vec();
        for inst in instances {
            self.remove_cell_instance(&inst);
        }

        // Remove all instances of this cell.
        let cell = &mut self.cells[cell_id.0];
        let references = std::mem::take(&mut cell.references)
            .into_values()
            .collect_vec();
        for inst in references {
            self.remove_cell_instance(&inst);
        }

        // Remove the cell.
        let name = self.cell(cell_id).name.clone();
        self.cells_by_name.remove(&name).unwrap();
        self.cells.remove(cell_id.0).unwrap();
    }

    /// Create a new instance of `cell_template` in the `parent` cell.
    fn create_cell_instance(
        &mut self,
        parent_id: &CellId,
        template_id: &CellId,
        name: Option<NameT>,
    ) -> CellInstId {
        {
            // Check that creating this cell instance does not create a cycle in the dependency graph.
            // There can be no recursive instances.
            let mut stack: SmallVec<[CellId; 4]> = smallvec![*parent_id];
            while let Some(c) = stack.pop() {
                if &c == template_id {
                    // The cell to be instantiated depends on the current cell.
                    // This would insert a loop into the dependency tree.
                    // TODO: Don't panic but return an `Err`.
                    panic!("Cannot create recursive instances.");
                }
                // Follow the dependent cells towards the root.
                stack.extend(self.cell(&c).dependent_cells.keys().copied())
            }
        }

        let inst = CellInst {
            name: name.clone(),
            template_cell_id: *template_id,
            parent_cell_id: *parent_id,
            properties: Default::default(),
            user_data: (),
            pins_ordered: Default::default(),
            transform: Default::default(),
            pin_instances: Default::default(),
            reference_id: Default::default(),
        };

        // Get mutable access to the parent cell AND immutable access to the temlate cell.
        let [parent_cell, template_cell] = self.cells.get_mut2([parent_id.0, template_id.0]);
        let parent_cell = parent_cell.expect("parent cell not found");
        let template_cell = template_cell.expect("template cell not found");

        // Insert the instance into the parent cell.
        let inst_id = CellInstId(*parent_id, parent_cell.instances.insert(inst));
        // Get mutable access to the instance.
        let inst = &mut parent_cell.instances[inst_id.1];

        // Create a bi-directional link between the template cell and the new instance.
        inst.reference_id = template_cell.references.insert(inst_id);

        let template_pins = template_cell.pins.iter().map(|p| PinId(*template_id, p.0));

        // Create pin instances from template pins.
        inst.pins_ordered = template_pins
            .map(|p| {
                let pin_inst = PinInst {
                    template_pin_id: p,
                    cell_inst: inst_id,
                    net: None,
                };
                PinInstId(inst_id, inst.pin_instances.insert(pin_inst))
            })
            .collect();

        if let Some(name) = name {
            // Create an entry for lookup by name.
            debug_assert!(
                !self.cell(parent_id).instances_by_name.contains_key(&name),
                "Cell instance name already exists."
            );
            self.cell_mut(parent_id)
                .instances_by_name
                .insert(name, inst_id);
        }

        // Remember dependency.
        {
            self.cell_mut(parent_id)
                .dependencies
                .entry(*template_id)
                .and_modify(|c| *c += 1)
                .or_insert(1);
        }

        // Remember dependency.
        {
            self.cell_mut(template_id)
                .dependent_cells
                .entry(*parent_id)
                .and_modify(|c| *c += 1)
                .or_insert(1);
        }

        inst_id
    }

    /// Remove a cell instance after disconnecting it from the nets.
    fn remove_cell_instance(&mut self, cell_inst_id: &CellInstId) {
        // Remove the instance name.
        self.rename_cell_instance(cell_inst_id, None);

        let (parent_id, template_id, reference_id) = {
            // Get access to the cell instance datastructure.
            let cell_inst = self.cell_inst(cell_inst_id);
            let reference_id = cell_inst.reference_id;
            let template_id = cell_inst.template_cell_id;

            let pins = cell_inst.pins_ordered.clone();

            // Disconnect all pins first.
            for pin in pins {
                self.disconnect_pin_instance(&pin);
            }

            (cell_inst_id.0, template_id, reference_id)
        };

        // Remove the instance and all references.

        let [parent, template] = self.cells.get_mut2([parent_id.0, template_id.0]);
        let parent_cell = parent.expect("parent cell not found");
        let template_cell = template.expect("template cell not found");

        // Remove dependency.
        {
            // Decrement counter.
            let count = parent_cell.dependencies.entry(template_id).or_insert(0); // Should not happen.
            *count -= 1;

            if *count == 0 {
                // Remove entry.
                parent_cell.dependencies.remove(&template_id);
            }
        }

        // Remove dependency.
        {
            // Decrement counter.
            let count = template_cell.dependent_cells.entry(parent_id).or_insert(0); // Should not happen.
            *count -= 1;

            if *count == 0 {
                // Remove entry.
                template_cell.dependent_cells.remove(&parent_id);
            }
        }

        parent_cell.instances.remove(cell_inst_id.1);
        // Remove the link from the template cell to this instance.
        template_cell.references.remove(reference_id);
    }

    /// Create a new net in the `parent` cell.
    fn create_net(&mut self, parent: &CellId, name: Option<NameT>) -> NetId {
        assert!(self.cells.contains_key(parent.0));

        let net = Net {
            name: name.clone(),
            pins: Default::default(),
            pin_instances: Default::default(),
            net_shapes: Default::default(),
        };
        let cell = self.cell_mut(parent);
        let id = NetId(*parent, cell.nets.insert(net));
        if let Some(name) = name {
            debug_assert!(
                !cell.nets_by_name.contains_key(&name),
                "Net name already exists."
            );
            cell.nets_by_name.insert(name, id);
        }
        id
    }

    /// Change the name of the net.
    fn rename_net(&mut self, net_id: &NetId, new_name: Option<NameT>) -> Option<NameT> {
        let parent_cell = self.parent_cell_of_net(net_id);

        // Check if a net with this name already exists.
        if let Some(name) = &new_name {
            if let Some(other) = self.cell(&parent_cell).nets_by_name.get(name) {
                if other != net_id {
                    panic!("Net name already exists.")
                } else {
                    // Name is the same as before.
                    return new_name;
                }
            }
        }

        let maybe_old_name = self.net_mut(net_id).name.take();

        let cell = self.cell_mut(&parent_cell);
        // Remove the old name mapping.
        if let Some(old_name) = &maybe_old_name {
            cell.nets_by_name.remove(old_name);
        }

        // Add the new name mapping.
        if let Some(new_name) = new_name {
            cell.nets
                .get_mut(net_id.1)
                .expect("Net not found.")
                .name
                .replace(new_name.clone());
            cell.nets_by_name.insert(new_name, *net_id);
        }

        maybe_old_name
    }

    /// Disconnect all connected terminals and remove the net.
    fn remove_net(&mut self, net: &NetId) {
        let parent_cell = net.0;

        assert_ne!(
            net,
            &self.net_zero(&parent_cell),
            "Cannot remove constant LOW net."
        );
        assert_ne!(
            net,
            &self.net_one(&parent_cell),
            "Cannot remove constant HIGH net."
        );

        // Remove all links from shapes to this net.
        let net_shapes = self.net(net).net_shapes.iter().copied().collect_vec();
        for net_shape in &net_shapes {
            self.set_net_of_shape(net_shape, None);
        }

        // Remove all links to pins.
        let pins = self.pins_for_net(net).collect_vec();
        let pin_insts = self.pins_instances_for_net(net).collect_vec();

        for p in pins {
            self.disconnect_pin(&p);
        }
        for p in pin_insts {
            self.disconnect_pin_instance(&p);
        }
        let name = self.net(net).name.clone();
        let cell = self.cell_mut(&parent_cell);
        cell.nets.remove(net.1);
        if let Some(name) = &name {
            cell.nets_by_name.remove(name).unwrap();
        }
    }

    /// Disconnect pin and return the ID of the net that was connected.
    fn disconnect_pin(&mut self, pin: &PinId) -> Option<NetId> {
        self.connect_pin(pin, None)
    }

    /// Connect the pin to a net.
    fn connect_pin(&mut self, pin: &PinId, net: Option<NetId>) -> Option<NetId> {
        if let Some(net) = net {
            // Sanity check.
            assert_eq!(
                self.pin(pin).cell,
                net.0,
                "Pin and net do not live in the same cell."
            );
        }

        let old_net = if let Some(net) = net {
            self.pin_mut(pin).net.replace(net)
        } else {
            self.pin_mut(pin).net.take()
        };

        if let Some(net) = old_net {
            // Remove the pin from the old net.
            self.net_mut(&net).pins.remove(pin);
        }

        if let Some(net) = net {
            // Store the pin in the new net.
            self.net_mut(&net).pins.insert(*pin);
        }

        old_net
    }

    /// Disconnect the pin instance and return the net to which it was connected.
    fn disconnect_pin_instance(&mut self, pin: &PinInstId) -> Option<NetId> {
        self.connect_pin_instance(pin, None)
    }

    /// Connect the pin to a net.
    fn connect_pin_instance(&mut self, pin: &PinInstId, net: Option<NetId>) -> Option<NetId> {
        if let Some(net) = net {
            assert_eq!(
                self.cell_inst(&self.pin_inst(pin).cell_inst).parent_cell_id,
                net.0,
                "Pin and net do not live in the same cell."
            );
        }

        let old_net = if let Some(net) = net {
            self.pin_inst_mut(pin).net.replace(net)
        } else {
            self.pin_inst_mut(pin).net.take()
        };

        if let Some(net) = old_net {
            // Remove the pin from the old net.
            self.net_mut(&net).pin_instances.remove(pin);
        }

        if let Some(net) = net {
            // Store the pin in the new net.
            self.net_mut(&net).pin_instances.insert(*pin);
        }

        old_net
    }

    /// Get a cell reference by its ID.
    fn cell(&self, id: &CellId) -> &Cell {
        &self.cells[id.0]
    }

    // /// Get a fat cell reference by its ID.
    // fn cell_ref(&self, id: &CellId) -> CellRef {
    //     CellRef {
    //         netlist: self,
    //         cell: &self.cells[id],
    //     }
    // }

    /// Get a mutable reference to the cell by its ID.
    fn cell_mut(&mut self, id: &CellId) -> &mut Cell {
        self.cells.get_mut(id.0).expect("Cell ID not found.")
    }

    /// Get a reference to a cell instance.
    fn cell_inst(&self, id: &CellInstId) -> &CellInst {
        &self.cells[id.0 .0].instances[id.1]
    }

    /// Get a mutable reference to a cell instance.
    fn cell_inst_mut(&mut self, id: &CellInstId) -> &mut CellInst {
        &mut self.cells[id.0 .0].instances[id.1]
    }

    /// Get a reference to a net by its ID.
    fn net(&self, id: &NetId) -> &Net {
        self.cells[id.0 .0]
            .nets
            .get(id.1)
            .expect("Net ID does not exist in this netlist.")
    }

    /// Get a mutable reference to a net by its ID.
    fn net_mut(&mut self, id: &NetId) -> &mut Net {
        self.cells[id.0 .0]
            .nets
            .get_mut(id.1)
            .expect("Net ID does not exist in this netlist.")
    }

    /// Get a reference to a pin by its ID.
    fn pin(&self, id: &PinId) -> &Pin {
        &self.cells[id.0 .0].pins[id.1]
    }

    /// Get a mutable reference to a pin by its ID.
    fn pin_mut(&mut self, id: &PinId) -> &mut Pin {
        self.cells[id.0 .0].pins.get_mut(id.1).unwrap()
    }

    /// Get a reference to a pin instance by its ID.
    fn pin_inst(&self, id: &PinInstId) -> &PinInst {
        &self.cell_inst(&id.0).pin_instances[id.1]
    }

    /// Get a mutable reference to a pin instance by its ID.
    fn pin_inst_mut(&mut self, id: &PinInstId) -> &mut PinInst {
        &mut self.cell_inst_mut(&id.0).pin_instances[id.1]
    }

    /// Append a new pin to the `parent` cell.
    /// Update all cell instances with the new pin.
    fn create_pin(&mut self, parent_id: CellId, name: NameT, direction: Direction) -> PinId {
        let pin = Pin {
            name,
            direction,
            cell: parent_id,
            net: Default::default(),
            pin_shapes: Default::default(),
        };

        let pin_id = PinId(
            parent_id,
            self.cells.get_mut(parent_id.0).unwrap().pins.insert(pin),
        );

        // Temporarily take ownership of the set of references.
        // Needed because of borrowing issues.
        let references = std::mem::take(&mut self.cells[parent_id.0].references);

        // Insert the pin in all instances of this cell.
        for inst_id in references.values() {
            // Create new pin instance.
            let pin_inst = PinInst {
                template_pin_id: pin_id,
                cell_inst: *inst_id,
                net: None,
            };
            // Get mutable access to the cell instance.
            let inst = &mut self.cells[inst_id.0 .0].instances[inst_id.1];
            let pin_inst_id = PinInstId(*inst_id, inst.pin_instances.insert(pin_inst));

            // Register the pin instance in the cell instance.
            inst.pins_ordered.push(pin_inst_id);
        }

        // Put back.
        self.cells[parent_id.0].references = references;

        pin_id
    }

    /// Iterate over all pins connected to a net.
    fn pins_for_net(&self, net: &NetId) -> impl Iterator<Item = PinId> + '_ {
        self.net(net).pins.iter().copied()
    }

    /// Iterate over all pin instances connected to a net.
    fn pins_instances_for_net(&self, net: &NetId) -> impl Iterator<Item = PinInstId> + '_ {
        self.net(net).pin_instances.iter().copied()
    }

    /// Get a mutable reference to a shape struct by its ID.
    fn shape_mut(&mut self, shape_id: &ShapeId) -> &mut Shape<Coord> {
        let ShapeId(cell, layer, shape_id) = shape_id;
        self.cell_mut(cell)
            .shapes_mut(layer)
            .expect("Layer not found.")
            .shapes
            .get_mut(*shape_id)
            .expect("Shape not found.")
    }

    /// Get a reference to a shape struct by its ID.
    fn shape(&self, shape_id: &ShapeId) -> &Shape<Coord> {
        let ShapeId(cell, layer, shape_id) = shape_id;
        self.cell(cell)
            .shapes(layer)
            .expect("Layer not found.")
            .shapes
            .get(*shape_id)
            .expect("Shape not found.")
    }
}

impl NetlistIds for Chip {
    type PinId = PinId;
    type PinInstId = PinInstId;
    type NetId = NetId;
}

impl NetlistBase for Chip {
    fn template_pin(&self, pin_instance: &Self::PinInstId) -> Self::PinId {
        self.pin_inst(pin_instance).template_pin_id
    }

    fn pin_direction(&self, pin: &Self::PinId) -> Direction {
        self.pin(pin).direction
    }

    fn pin_name(&self, pin: &Self::PinId) -> Self::NameType {
        self.pin(pin).name.clone()
    }

    fn pin_by_name(&self, parent_cell: &Self::CellId, name: &str) -> Option<Self::PinId> {
        // TODO: Create index for pin names.
        self.cell(parent_cell)
            .pins
            .iter()
            .find(|(_id, pin)| pin.name.as_str() == name)
            .map(|(id, _pin)| PinId(*parent_cell, id))
    }

    fn parent_cell_of_pin(&self, pin: &Self::PinId) -> Self::CellId {
        //self.pin(pin).cell
        pin.0
    }

    fn parent_of_pin_instance(&self, pin_inst: &Self::PinInstId) -> Self::CellInstId {
        self.pin_inst(pin_inst).cell_inst
    }

    fn parent_cell_of_net(&self, net: &Self::NetId) -> Self::CellId {
        net.0
    }

    fn net_of_pin(&self, pin: &Self::PinId) -> Option<Self::NetId> {
        self.pin(pin).net
    }

    fn net_of_pin_instance(&self, pin_inst: &Self::PinInstId) -> Option<Self::NetId> {
        self.pin_inst(pin_inst).net
    }

    fn net_zero(&self, parent_cell: &Self::CellId) -> Self::NetId {
        self.cell(parent_cell).net_low
    }

    fn net_one(&self, parent_cell: &Self::CellId) -> Self::NetId {
        self.cell(parent_cell).net_high
    }

    fn net_by_name(&self, parent_cell: &Self::CellId, name: &str) -> Option<Self::NetId> {
        self.cell(parent_cell).nets_by_name.get(name).copied()
    }

    fn net_name(&self, net: &Self::NetId) -> Option<Self::NameType> {
        self.net(net).name.clone()
    }

    fn for_each_pin<F>(&self, cell: &Self::CellId, f: F)
    where
        F: FnMut(Self::PinId),
    {
        self.cell(cell)
            .pins
            .iter()
            .map(|p| PinId(*cell, p.0))
            .for_each(f)
    }

    fn each_pin(&self, cell_id: &CellId) -> Box<dyn Iterator<Item = PinId> + '_> {
        let cell_id = *cell_id;
        Box::new(
            self.cell(&cell_id)
                .pins
                .iter()
                .map(move |p| PinId(cell_id, p.0)),
        )
    }

    fn for_each_pin_instance<F>(&self, cell_inst: &Self::CellInstId, f: F)
    where
        F: FnMut(Self::PinInstId),
    {
        self.cell_inst(cell_inst)
            .pins_ordered
            .iter()
            .copied()
            .for_each(f)
    }

    fn each_pin_instance<'a>(
        &'a self,
        cell_inst: &Self::CellInstId,
    ) -> Box<dyn Iterator<Item = Self::PinInstId> + 'a> {
        Box::new(self.cell_inst(cell_inst).pins_ordered.iter().copied())
    }

    fn for_each_internal_net<F>(&self, cell: &Self::CellId, f: F)
    where
        F: FnMut(Self::NetId),
    {
        self.cell(cell)
            .nets
            .iter()
            .map(|(id, _)| NetId(*cell, id))
            .for_each(f)
    }

    fn each_internal_net(&self, cell: &Self::CellId) -> Box<dyn Iterator<Item = Self::NetId> + '_> {
        let cell = *cell;
        Box::new(
            self.cell(&cell)
                .nets
                .iter()
                .map(move |(id, _)| NetId(cell, id)),
        )
    }

    fn num_internal_nets(&self, cell: &Self::CellId) -> usize {
        self.cell(cell).nets.len()
    }

    fn num_pins(&self, cell: &Self::CellId) -> usize {
        self.cell(cell).pins.len()
    }

    fn for_each_pin_of_net<F>(&self, net: &Self::NetId, f: F)
    where
        F: FnMut(Self::PinId),
    {
        self.net(net).pins.iter().copied().for_each(f)
    }

    fn each_pin_of_net<'a>(
        &'a self,
        net: &Self::NetId,
    ) -> Box<dyn Iterator<Item = Self::PinId> + 'a> {
        Box::new(self.net(net).pins.iter().copied())
    }

    fn for_each_pin_instance_of_net<F>(&self, net: &Self::NetId, f: F)
    where
        F: FnMut(Self::PinInstId),
    {
        self.net(net).pin_instances.iter().copied().for_each(f)
    }

    fn each_pin_instance_of_net<'a>(
        &'a self,
        net: &Self::NetId,
    ) -> Box<dyn Iterator<Item = Self::PinInstId> + 'a> {
        Box::new(self.net(net).pin_instances.iter().copied())
    }
}

impl NetlistEdit for Chip {
    fn create_pin(
        &mut self,
        cell: &Self::CellId,
        name: Self::NameType,
        direction: Direction,
    ) -> Self::PinId {
        Chip::create_pin(self, *cell, name, direction)
    }

    fn remove_pin(&mut self, id: &Self::PinId) {
        // Remove all links from shapes to this pin.
        let pin_shapes = self.pin(id).pin_shapes.iter().cloned().collect_vec();
        for pin_shape in &pin_shapes {
            self.set_pin_of_shape(pin_shape, None);
        }

        // Disconnect the pin for all instances.
        let cell = self.parent_cell_of_pin(id);
        for inst in self.each_cell_instance_vec(&cell) {
            let pin_inst = self.pin_instance(&inst, id);
            self.disconnect_pin_instance(&pin_inst);

            // Remove pin instance.
            self.cell_inst_mut(&inst)
                .pins_ordered
                .retain(|p| p != &pin_inst);
            // Delete the pin instance struct.
            self.cell_inst_mut(&inst).pin_instances.remove(pin_inst.1);
        }

        // Disconnect this pin from internal nets.
        self.disconnect_pin(id);

        // Remove the pin from the cell.
        self.cell_mut(&cell).pins_ordered.retain(|p| p != id);
        self.cell_mut(&cell).pins.remove(id.1);

        // Delete the pin struct.
        self.cell_mut(&cell).pins.remove(id.1);
    }

    fn rename_pin(&mut self, pin: &Self::PinId, new_name: Self::NameType) -> Self::NameType {
        let cell = self.parent_cell_of_pin(pin);
        let existing = self.pin_by_name(&cell, &new_name);
        if existing.is_some() {
            panic!(
                "Pin name already exists in cell '{}': '{}'",
                self.cell_name(&cell),
                new_name
            )
        }
        let old_name = std::mem::replace(&mut self.pin_mut(pin).name, new_name);
        old_name
    }

    fn create_net(&mut self, parent: &CellId, name: Option<Self::NameType>) -> NetId {
        Chip::create_net(self, parent, name)
    }

    fn rename_net(
        &mut self,
        net_id: &Self::NetId,
        new_name: Option<Self::NameType>,
    ) -> Option<Self::NameType> {
        Chip::rename_net(self, net_id, new_name)
    }

    fn remove_net(&mut self, net: &NetId) {
        Chip::remove_net(self, net)
    }

    fn connect_pin(&mut self, pin: &PinId, net: Option<NetId>) -> Option<NetId> {
        Chip::connect_pin(self, pin, net)
    }

    fn connect_pin_instance(&mut self, pin: &PinInstId, net: Option<NetId>) -> Option<Self::NetId> {
        Chip::connect_pin_instance(self, pin, net)
    }
}

#[test]
fn test_create_populated_netlist() {
    let mut netlist = Chip::default();
    let top = netlist.create_cell("TOP".into(), vec![("A".into(), Direction::Input)]);
    netlist.create_pin(top, "B".into(), Direction::Output);
    assert_eq!(Some(top), netlist.cell_by_name("TOP"));

    let sub_a = netlist.create_cell(
        "SUB_A".into(),
        vec![
            ("A".into(), Direction::Input),
            ("B".into(), Direction::Output),
        ],
    );
    let sub_b = netlist.create_cell(
        "SUB_B".into(),
        vec![
            ("A".into(), Direction::Input),
            ("B".into(), Direction::Output),
        ],
    );

    let inst_a = netlist.create_cell_instance(&top, &sub_a, None);
    let _inst_b = netlist.create_cell_instance(&top, &sub_b, None);

    let net_a = netlist.create_net(&top, Some("NetA".into()));
    let net_b = netlist.create_net(&top, Some("NetB".into()));

    let pins_a = netlist.each_pin_instance(&inst_a).collect_vec();
    let pins_top = netlist.each_pin(&top).collect_vec();

    netlist.connect_pin_instance(&pins_a[0], Some(net_a));
    netlist.connect_pin_instance(&pins_a[1], Some(net_b));

    netlist.connect_pin(&pins_top[0], Some(net_a));
    netlist.connect_pin(&pins_top[1], Some(net_b));

    assert_eq!(netlist.num_net_terminals(&net_a), 2);
    assert_eq!(netlist.num_net_terminals(&net_b), 2);
}

/// Wrapper around a `Geometry` struct.
#[derive(Clone, Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Shape<C, U = ()> {
    /// The geometry of this shape.
    geometry: Geometry<C>,
    // /// Reference ID to container.
    // parent_id: Index<Shapes<T>>,
    /// Net attached to this shape.
    net: Option<NetId>,
    /// Pin that belongs to this shape.
    pin: Option<PinId>,
    /// User-defined data.
    #[allow(unused)]
    user_data: U,
}

/// `Shapes<T>` is a collection of `Shape<T>` structs. Each of
/// the elements is assigned an index when inserted into the collection.
#[derive(Clone, Debug, Default)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
struct Shapes<C>
where
    C: CoordinateType,
{
    /// Shape elements.
    shapes: SlabAlloc<Shape<C>, u32, SlabId>,
    /// Property stores for the shapes.
    shape_properties: IntHashMap<SlabIndex<u32, SlabId>, PropertyStore<NameT>>,
}

impl<C: CoordinateType> Shapes<C> {
    /// Iterate over all shapes in this container.
    fn each_shape(&self) -> impl Iterator<Item = &Shape<C>> {
        self.shapes.values()
    }
}

impl<C: CoordinateType> TryBoundingBox<C> for Shapes<C> {
    fn try_bounding_box(&self) -> Option<Rect<C>> {
        // Compute the bounding box from scratch.

        self.each_shape().fold(None, |bbox, shape| {
            let bbox_new = shape.geometry.try_bounding_box();
            match bbox {
                None => bbox_new,
                Some(bbox1) => {
                    Some(
                        // Compute bounding box of the two rectangles.
                        match bbox_new {
                            None => bbox1,
                            Some(bbox2) => bbox1.add_rect(&bbox2),
                        },
                    )
                }
            }
        })
    }
}

impl HierarchyIds for Chip<Coord> {
    type CellId = CellId;
    type CellInstId = CellInstId;
}

impl HierarchyBase for Chip<Coord> {
    type NameType = NameT;

    fn cell_by_name(&self, name: &str) -> Option<CellId> {
        Chip::cell_by_name(self, name)
    }

    fn cell_instance_by_name(
        &self,
        parent_cell: &Self::CellId,
        name: &str,
    ) -> Option<Self::CellInstId> {
        self.cell(parent_cell).instances_by_name.get(name).copied()
    }

    fn cell_name(&self, cell: &Self::CellId) -> Self::NameType {
        self.cell(cell).name.clone()
    }

    fn cell_instance_name(&self, cell_inst: &Self::CellInstId) -> Option<Self::NameType> {
        self.cell_inst(cell_inst).name.clone()
    }

    fn parent_cell(&self, cell_instance: &Self::CellInstId) -> Self::CellId {
        cell_instance.0
    }

    fn template_cell(&self, cell_instance: &Self::CellInstId) -> Self::CellId {
        self.cell_inst(cell_instance).template_cell_id
    }

    fn for_each_cell<F>(&self, f: F)
    where
        F: FnMut(Self::CellId),
    {
        self.cells.keys().map(CellId).for_each(f)
    }

    fn each_cell(&self) -> Box<dyn Iterator<Item = CellId> + '_> {
        Box::new(self.cells.keys().map(CellId))
    }

    fn for_each_cell_instance<F>(&self, parent: &Self::CellId, f: F)
    where
        F: FnMut(Self::CellInstId),
    {
        self.cell(parent)
            .instances
            .iter()
            .map(|(id, _)| CellInstId(*parent, id))
            .for_each(f)
    }

    fn each_cell_instance(
        &self,
        parent: &Self::CellId,
    ) -> Box<dyn Iterator<Item = Self::CellInstId> + '_> {
        let parent = *parent;
        Box::new(
            self.cell(&parent)
                .instances
                .iter()
                .map(move |(id, _)| CellInstId(parent, id)),
        )
    }

    fn for_each_cell_dependency<F>(&self, cell: &Self::CellId, f: F)
    where
        F: FnMut(Self::CellId),
    {
        self.cell(cell).dependencies.keys().copied().for_each(f);
    }

    fn each_cell_dependency(
        &self,
        cell: &Self::CellId,
    ) -> Box<dyn Iterator<Item = Self::CellId> + '_> {
        Box::new(self.cell(cell).dependencies.keys().copied())
    }

    fn num_cell_dependencies(&self, cell: &Self::CellId) -> usize {
        self.cell(cell).dependencies.len()
    }

    fn for_each_dependent_cell<F>(&self, cell: &Self::CellId, f: F)
    where
        F: FnMut(Self::CellId),
    {
        self.cell(cell).dependent_cells.keys().copied().for_each(f);
    }

    fn each_dependent_cell(
        &self,
        cell: &Self::CellId,
    ) -> Box<dyn Iterator<Item = Self::CellId> + '_> {
        Box::new(self.cell(cell).dependent_cells.keys().copied())
    }

    fn num_dependent_cells(&self, cell: &Self::CellId) -> usize {
        self.cell(cell).dependent_cells.len()
    }

    fn for_each_cell_reference<F>(&self, cell: &Self::CellId, f: F)
    where
        F: FnMut(Self::CellInstId),
    {
        self.cell(cell).references.values().copied().for_each(f)
    }

    fn each_cell_reference(
        &self,
        cell: &Self::CellId,
    ) -> Box<dyn Iterator<Item = Self::CellInstId> + '_> {
        Box::new(self.cell(cell).references.values().copied())
    }

    fn num_cell_references(&self, cell: &Self::CellId) -> usize {
        self.cell(cell).references.len()
    }

    fn num_child_instances(&self, cell: &Self::CellId) -> usize {
        self.cell(cell).instances.len()
    }

    fn num_cells(&self) -> usize {
        self.cells.len()
    }

    fn get_chip_property(&self, key: &Self::NameType) -> Option<PropertyValue> {
        self.properties.get(key).cloned()
    }

    fn get_cell_property(
        &self,
        cell: &Self::CellId,
        key: &Self::NameType,
    ) -> Option<PropertyValue> {
        self.cell(cell).properties.get(key).cloned()
    }

    fn get_cell_instance_property(
        &self,
        inst: &Self::CellInstId,
        key: &Self::NameType,
    ) -> Option<PropertyValue> {
        self.cell_inst(inst).properties.get(key).cloned()
    }
}

impl LayoutIds for Chip<Coord> {
    type Coord = Coord;
    type Area = Area;
    type LayerId = LayerId;
    type ShapeId = ShapeId;
}

impl LayoutBase for Chip<Coord> {
    fn dbu(&self) -> Self::Coord {
        self.dbu
    }

    fn each_layer(&self) -> Box<dyn Iterator<Item = Self::LayerId> + '_> {
        Box::new(self.layer_info.iter().map(|(id, _)| LayerId(id)))
    }

    fn layer_info(&self, layer: &Self::LayerId) -> LayerInfo<Self::NameType> {
        self.layer_info[layer.0].clone()
    }

    fn find_layer(&self, index: u32, datatype: u32) -> Option<Self::LayerId> {
        self.layers_by_index_datatype
            .get(&(index, datatype))
            .copied()
    }

    fn layer_by_name(&self, name: &str) -> Option<Self::LayerId> {
        self.layers_by_name.get(name).cloned()
    }

    fn bounding_box_per_layer(
        &self,
        cell: &Self::CellId,
        layer: &Self::LayerId,
    ) -> Option<Rect<Coord>> {
        // Compute the bounding box of the cell's own shapes.
        let mut bbox = self
            .cell(cell)
            .shapes(layer)
            .and_then(|shapes| shapes.try_bounding_box());
        // Update the bounding box with the children cells.
        self.for_each_cell_instance(cell, |i| {
            let template = self.template_cell(&i);
            let tf = self.get_transform(&i);
            let child_bbox = self
                .bounding_box_per_layer(&template, layer)
                // Transform the child bounding box to ther correct position.
                .map(|b| b.transform(|p| tf.transform_point(p)));

            bbox = match (bbox, child_bbox) {
                (None, None) => None,
                (Some(b), None) | (None, Some(b)) => Some(b),
                (Some(a), Some(b)) => Some(a.add_rect(&b)),
            }
        });

        bbox
    }

    fn each_shape_id(
        &self,
        cell: &Self::CellId,
        layer: &Self::LayerId,
    ) -> Box<dyn Iterator<Item = Self::ShapeId> + '_> {
        if let Some(shapes) = self.cell(cell).shapes(layer) {
            let (cell, layer) = (*cell, *layer);
            Box::new(
                shapes
                    .shapes
                    .iter()
                    .map(move |(id, _)| ShapeId(cell, layer, id)),
            )
        } else {
            Box::new(None.into_iter()) // Empty iterator.
        }
    }

    // fn each_shape(&self, cell: &Self::CellId, layer: &Self::LayerId) -> Box<dyn Iterator<Item=&Geometry<Self::Coord>> + '_> {
    //     Box::new(self.cells[cell].shapes_map[layer].shapes.values().map(|s| &s.geometry))
    // }

    fn for_each_shape<F>(&self, cell_id: &Self::CellId, layer: &Self::LayerId, mut f: F)
    where
        F: FnMut(&Self::ShapeId, &Geometry<Self::Coord>),
    {
        if self.layer_info.get(layer.0).is_none() {
            // Layer does not exist.
            // TODO: panic or what?
            panic!("layer ID not found");
        }

        self.cells[cell_id.0]
            .shapes_map
            .get(layer.0.index())
            .into_iter()
            .for_each(|s| {
                s.shapes
                    .iter()
                    .for_each(|(id, s)| f(&ShapeId(*cell_id, *layer, id), &s.geometry));
            })
    }

    fn with_shape<F, R>(&self, shape_id: &Self::ShapeId, mut f: F) -> R
    where
        F: FnMut(&Self::LayerId, &Geometry<Self::Coord>) -> R,
    {
        let shape = self.shape(shape_id);
        f(&shape_id.1, &shape.geometry)
    }

    fn parent_of_shape(&self, shape_id: &Self::ShapeId) -> (Self::CellId, Self::LayerId) {
        self.cells[shape_id.0 .0]
            .shapes(&shape_id.1)
            .expect("Layer does not exist anymore.")
            .shapes
            .get(shape_id.2)
            .expect("Shape does not exist anymore.");
        (shape_id.0, shape_id.1)
    }

    fn get_transform(&self, cell_inst: &Self::CellInstId) -> SimpleTransform<Self::Coord> {
        *self.cell_inst(cell_inst).get_transform()
    }

    fn get_shape_property(
        &self,
        shape: &Self::ShapeId,
        key: &Self::NameType,
    ) -> Option<PropertyValue> {
        let ShapeId(cell, layer, id) = shape;

        if self.layer_info.get(layer.0).is_none() {
            // Layer does not exist.
            // TODO: panic or what?
            panic!("layer ID is invalid");
        }

        self.cell(cell).shapes_map[layer.0.index()]
            .shape_properties
            .get(id)
            .and_then(|props| props.get(key))
            .cloned()
    }
}

impl HierarchyEdit for Chip<Coord> {
    fn create_cell(&mut self, name: Self::NameType) -> Self::CellId {
        self.create_cell(name, vec![])
    }

    fn remove_cell(&mut self, cell_id: &Self::CellId) {
        self.remove_cell(cell_id)
    }

    fn create_cell_instance(
        &mut self,
        parent_cell: &Self::CellId,
        template_cell: &Self::CellId,
        name: Option<Self::NameType>,
    ) -> Self::CellInstId {
        self.create_cell_instance(parent_cell, template_cell, name)
    }

    fn remove_cell_instance(&mut self, id: &Self::CellInstId) {
        <Chip<Coord>>::remove_cell_instance(self, id)
    }

    fn rename_cell_instance(&mut self, inst: &Self::CellInstId, new_name: Option<Self::NameType>) {
        <Chip<Coord>>::rename_cell_instance(self, inst, new_name)
    }

    fn rename_cell(&mut self, cell: &Self::CellId, new_name: Self::NameType) {
        <Chip<Coord>>::rename_cell(self, cell, new_name)
    }

    fn set_chip_property(&mut self, key: Self::NameType, value: PropertyValue) {
        self.properties.insert(key, value);
    }

    fn set_cell_property(
        &mut self,
        cell: &Self::CellId,
        key: Self::NameType,
        value: PropertyValue,
    ) {
        self.cell_mut(cell).properties.insert(key, value);
    }

    fn set_cell_instance_property(
        &mut self,
        inst: &Self::CellInstId,
        key: Self::NameType,
        value: PropertyValue,
    ) {
        self.cell_inst_mut(inst).properties.insert(key, value);
    }
}

impl LayoutEdit for Chip<Coord> {
    fn set_dbu(&mut self, dbu: Self::Coord) {
        self.dbu = dbu;
    }

    fn create_layer(&mut self, index: u32, datatype: u32) -> Self::LayerId {
        let info = LayerInfo {
            index,
            datatype,
            name: None,
        };

        let layer_id = LayerId(self.layer_info.insert(info));

        // Create new entries in the layer lookup tables.
        self.layers_by_index_datatype
            .insert((index, datatype), layer_id);
        layer_id
    }

    fn create_layer_with_id(
        &mut self,
        layer_id: Self::LayerId,
        index: u32,
        datatype: u32,
    ) -> Result<(), ()> {
        if self.layer_info.get(layer_id.0).is_some() {
            return Err(());
        }

        let info = LayerInfo {
            index,
            datatype,
            name: None,
        };

        self.layer_info.insert_at(layer_id.0, info).unwrap();

        // Create new entries in the layer lookup tables.
        self.layers_by_index_datatype
            .insert((index, datatype), layer_id);

        Ok(())
    }

    fn set_layer_name(
        &mut self,
        layer: &Self::LayerId,
        name: Option<Self::NameType>,
    ) -> Option<Self::NameType> {
        if let Some(name) = &name {
            // Check that we do not shadow another layer name.
            let existing = self.layers_by_name.get(name);

            if existing == Some(layer) {
                // Nothing to be done.
                return Some(name.clone());
            }

            if existing.is_some() {
                panic!("Layer name already exists: '{}'", name)
            }
        }

        // Remove the name.
        let previous_name = self
            .layer_info
            .get_mut(layer.0)
            .expect("Layer ID not found.")
            .name
            .take();
        if let Some(prev_name) = &previous_name {
            // Remove the name from the table.
            self.layers_by_name.remove(prev_name);
        }

        if let Some(name) = name {
            // Create the link from the name to the ID.
            self.layers_by_name.insert(name.clone(), *layer);
            // Store the name.
            self.layer_info
                .get_mut(layer.0)
                .expect("Layer ID not found.")
                .name = Some(name);
        }
        previous_name
    }

    fn insert_shape(
        &mut self,
        parent_cell: &Self::CellId,
        layer: &Self::LayerId,
        geometry: Geometry<Self::Coord>,
    ) -> Self::ShapeId {
        let shape = Shape {
            geometry,
            net: None,
            pin: None,
            user_data: Default::default(),
        };

        let id = self
            .cell_mut(parent_cell)
            .get_or_create_shapes_mut(layer)
            .shapes
            .insert(shape);

        ShapeId(*parent_cell, *layer, id)
    }

    fn remove_shape(&mut self, shape_id: &Self::ShapeId) -> Option<Geometry<Self::Coord>> {
        // Remove all links to this shape.
        if let Some(net) = self.get_net_of_shape(shape_id) {
            self.net_mut(&net).net_shapes.remove(shape_id);
        }
        if let Some(pin) = self.get_pin_of_shape(shape_id) {
            self.pin_mut(&pin).pin_shapes.remove(shape_id);
        }

        let ShapeId(parent_cell, layer, _) = *shape_id;

        self.cell_mut(&parent_cell)
            .shapes_mut(&layer)
            .expect("Layer not found.")
            .shapes
            .remove(shape_id.2)
            .map(|s| s.geometry)
    }

    fn replace_shape(
        &mut self,
        shape_id: &Self::ShapeId,
        geometry: Geometry<Self::Coord>,
    ) -> Geometry<Self::Coord> {
        let ShapeId(parent_cell, layer, _) = *shape_id;

        let g = &mut self
            .cell_mut(&parent_cell)
            .shapes_mut(&layer)
            .expect("Layer not found.")
            .shapes
            .get_mut(shape_id.2)
            .expect("Shape not found.")
            .geometry;
        let mut new_g = geometry;
        std::mem::swap(g, &mut new_g);
        new_g
    }

    fn set_transform(&mut self, cell_inst: &Self::CellInstId, tf: SimpleTransform<Self::Coord>) {
        self.cell_inst_mut(cell_inst).set_transform(tf)
    }

    fn set_shape_property(
        &mut self,
        shape: &Self::ShapeId,
        key: Self::NameType,
        value: PropertyValue,
    ) {
        let ShapeId(parent_cell, layer, id) = *shape;
        if self.layer_info.get(layer.0).is_none() {
            // Layer does not exist.
            // TODO: panic or what?
            panic!("layer ID is invalid");
        }
        self.cell_mut(&parent_cell).shapes_map[layer.0.index()]
            .shape_properties
            .entry(id)
            .or_default()
            .insert(key, value);
    }
}

impl L2NBase for Chip<Coord> {
    fn shapes_of_net(&self, net_id: &Self::NetId) -> Box<dyn Iterator<Item = Self::ShapeId> + '_> {
        Box::new(self.net(net_id).net_shapes.iter().copied())
    }

    fn shapes_of_pin(&self, pin_id: &Self::PinId) -> Box<dyn Iterator<Item = Self::ShapeId> + '_> {
        Box::new(self.pin(pin_id).pin_shapes.iter().cloned())
    }

    fn get_net_of_shape(&self, shape_id: &Self::ShapeId) -> Option<Self::NetId> {
        self.shape(shape_id).net
    }

    fn get_pin_of_shape(&self, shape_id: &Self::ShapeId) -> Option<Self::PinId> {
        self.shape(shape_id).pin
    }
}

impl L2NEdit for Chip<Coord> {
    fn set_pin_of_shape(
        &mut self,
        shape_id: &Self::ShapeId,
        pin: Option<Self::PinId>,
    ) -> Option<Self::PinId> {
        // Create link from pin to shape.
        if let Some(pin) = &pin {
            let not_yet_present = self.pin_mut(pin).pin_shapes.insert(*shape_id);
            if !not_yet_present {
                // The shape is already assigned to this pin. Don't do anything more.
                return Some(*pin);
            }
        }

        // Create from shape to link.
        let mut pin = pin;
        std::mem::swap(&mut self.shape_mut(shape_id).pin, &mut pin);
        let previous_pin = pin;

        // Remove the old link to the pin.
        if let Some(previous_pin) = &previous_pin {
            assert!(
                self.pin_mut(previous_pin).pin_shapes.remove(shape_id),
                "Pin was not linked to the shape."
            );
        }

        // Return the previous pin (got it by the above swap operation).
        previous_pin
    }

    fn set_net_of_shape(
        &mut self,
        shape_id: &Self::ShapeId,
        net: Option<Self::NetId>,
    ) -> Option<Self::NetId> {
        // Create link from net to shape.
        if let Some(net) = &net {
            let not_yet_present = self.net_mut(net).net_shapes.insert(*shape_id);
            if !not_yet_present {
                // The shape is already assigned to this net. Don't do anything more.
                return Some(*net);
            }
        }

        // Create from shape to link.
        let mut net = net;
        std::mem::swap(&mut self.shape_mut(shape_id).net, &mut net);
        let previous_net = net;

        // Remove the old link to the net.
        if let Some(previous_net) = &previous_net {
            assert!(
                self.net_mut(previous_net).net_shapes.remove(shape_id),
                "Net was not linked to the shape."
            );
        }

        // Return the previous net (got it by the above swap operation).
        previous_net
    }
}
